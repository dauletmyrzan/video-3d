<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
        'prefix' => '{locale}',
        'where' => ['locale' => '^ru|en$'],
        'middleware' => ['web', 'setlocale']
    ], function() {
    Route::get('/', 'HomeController@index');

    Route::get('/about', 'HomeController@about')->name('about');
    Route::get('/reviews', 'HomeController@reviews')->name('reviews');
    Route::get('/video-types', 'HomeController@videoTypes')->name('video_types');

    Route::middleware(['auth', 'verified'])->group(function(){
        Route::get('/help', 'FaqController@help')->name('help');
        Route::get('/my-videos', [
            'uses' => 'VideoController@myVideos',
            'as' => 'my_videos',
        ]);
        Route::get('/profile', [
            'uses' => 'UserController@edit',
            'as' => 'profile',
        ]);
        Route::get('/my', [
            'uses' => 'UserController@index',
            'as' => 'my',
        ]);
        Route::get('/my-videos/{id}/feedback', [
            'uses' => 'VideoController@feedback'
        ]);
        Route::get('/payment/{id}', 'UserController@payment');
    });

    Route::get('/how-it-works', 'HomeController@howItWorks')->name('how_it_works');
    Route::get('/2d-video', 'HomeController@video2d');
    Route::get('/3d-video', 'HomeController@video3d');
    Route::get('/presentation', 'HomeController@videoPresentation');
    Route::get('/promo-video', 'HomeController@videoPromo');


    Route::get('/steps/congratulations', 'PollController@congratulations');
    Route::get('/steps/{code}', 'HomeController@step');
});

Auth::routes(['verify' => true]);

Route::get('/', function () {
    return redirect(app()->getLocale());
});

Route::group([
    'prefix' => '{locale}',
    'where' => ['locale' => '^ru|en$'],
    'middleware' => ['auth', 'verified', 'web', 'setlocale']
], function() {
    Route::get('/payment', 'HomeController@payment');
});

Route::middleware(['auth', 'verified'])->group(function(){
    Route::post('/payment/invoice', 'UserController@generateInvoice');
    Route::post('/set_script_status', [
        'uses' => 'AdminController@setScriptStatus',
        'as' => 'set_script_status'
    ]);
    Route::post('/set_video_status', [
        'uses' => 'AdminController@setVideoStatus',
        'as' => 'set_video_status'
    ]);
    Route::post('/upload_new_script', [
        'uses' => 'AdminController@uploadNewScript',
        'as' => 'upload_new_script'
    ]);
    Route::post('/upload_new_video', [
        'uses' => 'AdminController@uploadNewVideo',
        'as' => 'upload_new_video'
    ]);
    Route::post('/create_script', [
        'uses' => 'AdminController@createScript',
        'as' => 'create_script'
    ]);
    Route::post('/create_video', [
        'uses' => 'AdminController@createVideo',
        'as' => 'create_video'
    ]);
    Route::post('/upload_photo', [
        'uses' => 'UserController@uploadPhoto'
    ]);

    Route::get('/delete_photo', [
        'uses' => 'UserController@deletePhoto'
    ]);

    Route::post('/save_personal_data', [
        'uses' => 'UserController@savePersonalData'
    ]);

    Route::post('/new_password', [
        'uses' => 'UserController@newPassword'
    ]);

    Route::post('/start_shooting', [
        'uses' => 'ScriptController@startShooting'
    ]);

    Route::post('/approve_video', [
        'uses' => 'VideoController@approveVideo'
    ]);
    Route::post('/video_feedback', [
        'uses' => 'VideoController@sendFeedback'
    ]);

    Route::post('/ask_question', [
        'uses' => 'FaqController@ask'
    ]);
    Route::get('/get_timeline', 'UserController@getTimeline');
});

Route::middleware(['auth', 'admin'])->prefix('admin')->group(function(){
    Route::get('/', [
        'uses' => 'AdminController@index',
        'as' => 'admin'
    ]);
    Route::get('/polls', [
        'uses' => 'AdminController@polls',
        'as' => 'polls'
    ]);
    Route::get('/polls/{id}', [
        'uses' => 'AdminController@poll'
    ]);

    Route::get('/scripts', [
        'uses' => 'AdminController@scripts',
        'as' => 'scripts'
    ]);
    Route::get('/scripts/new', [
        'uses' => 'AdminController@newScript'
    ]);
    Route::get('/scripts/{id}', [
        'uses' => 'AdminController@script'
    ]);
    Route::get('/scripts/{id}/feedback', [
        'uses' => 'AdminController@scriptFeedback'
    ]);

    Route::get('/videos', [
        'uses' => 'AdminController@videos',
        'as' => 'videos'
    ]);
    Route::get('/videos/new', [
        'uses' => 'AdminController@newVideo'
    ]);
    Route::get('/videos/{id}', [
        'uses' => 'AdminController@video'
    ]);
    Route::get('/videos/{id}/feedback', [
        'uses' => 'AdminController@videoFeedback'
    ]);

    Route::post('/answer_script_feedback', [
        'uses' => 'AdminController@answerScriptFeedback',
        'as' => 'answer_script_feedback'
    ]);
    Route::post('/answer_video_feedback', [
        'uses' => 'AdminController@answerVideoFeedback',
        'as' => 'answer_video_feedback'
    ]);


    Route::get('/faq', [
        'uses' => 'AdminController@faq'
    ]);
    Route::get('/faq/{id}/edit', [
        'uses' => 'AdminController@editFaq'
    ]);
    Route::post('/update_faq', [
        'uses' => 'AdminController@updateFaq',
        'as' => 'update_faq'
    ]);
    Route::get('/faq/create', [
        'uses' => 'AdminController@createFaq',
        'as' => 'create_faq'
    ]);
    Route::post('/store_faq', [
        'uses' => 'AdminController@storeFaq',
        'as' => 'store_faq'
    ]);

    Route::get('/faq/questions', [
        'uses' => 'AdminController@questions'
    ]);

    Route::get('/clients', [
        'uses' => 'AdminController@clients'
    ]);

    Route::get('/clients/{id}', [
        'uses' => 'AdminController@client'
    ]);

    Route::get('/videos/{id}/upload-full', 'AdminController@uploadFullIndex');

});

Route::post('/post-steps/{code}', 'PollController@storeStep');
Route::post('/steps-upload-file', 'PollController@uploadFile');
Route::post('/saveStepsAndRegister', 'PollController@saveStepsAndRegister');