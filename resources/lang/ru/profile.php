<?php
return [
    'account' => 'Личный кабинет',
    'h1' => 'Профиль',
    'set_photo' => 'Установить фото профиля',
    'delete_photo' => 'Удалить фото',
    'personal_data' => 'Личные данные',
    'change_password' => 'Изменить пароль',
    'sign_out' => 'Выйти'

];