<?php

return [
    'home' => 'Главная',
    'portfolio' => 'Выполненные работы',
    'reviews' => 'Отзывы',
    'my_scripts' => 'Мои сценарии',
    'chat' => 'Чат с режиссером',
    'my_videos' => 'Мои ролики',
    'how_it_works' => 'Как это работает?',
    'video_types' => 'Виды видеороликов',
    'about' => 'О нас',
    'help' => 'Помощь',
];