<?php

return [
    'h1' => "<span style='color:#ffcb4d;'>2D</span> анимационный ролик в Казахстане",
    'h2' => "2D видеоролики <span class='text-success'>ТОП-5</span> преимуществ",
    'strength1_title' => 'Низкая цена',
    'strength1_desc'  => 'Несмотря на сложную работу, производство анимационных 2D роликов стоит не дорого. Благодаря нашей собственной команде художников, и моушэн дизайнеров, мы можем нарисовать очень красивые иллюстрации.',
    'strength2_title' => 'Неограниченная фантазия',
    'strength2_desc'  => 'Креаторы могут придумать любую идею. Мы можем выстроить сценарий для любого вида деятельности, и обрисовать всё что будет интересно вашим клиентам.',
    'strength3_title' => 'Качественная анимация',
    'strength3_desc'  => 'Мы 145 часов проводим над тем, чтобы анимация деталей, персонажей, продуктов была максимально плавной. Приятный сюжет, вместе с захватывающей картинкой – залог успешного видеоролика.',
    'strength4_title' => 'Дикторы-звезды',
    'strength4_desc'  => 'В нашей команде дикторы актеры дубляжей таких фильмов как «Аватар», «Властелин Колец» и «Игра Престолов»',
    'strength5_title' => 'Привлекает внимание',
    'strength5_desc'  => 'Мультипликации намного лучше захватывают внимание, чем съемка камерой.',
    'see_more'        => '<span class="text-success">Примеры</span> наших работ',
    'special_title'   => 'Акция',
    'special_desc'    => 'Заполни бриф и получи сценарий от профессиональной команды сценаристов и художников - <span class="text-orange">БЕСПЛАТНО</span>'
];