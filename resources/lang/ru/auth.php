<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Введенные данные неверны.',
    'throttle' => 'Слишком много попыток. Пожалуйста, повторите через :seconds сек.',
    'login' => 'Войти',
    'email_placeholder' => 'Введите ваш email',
    'enter_password_placeholder' => 'Введите ваш пароль',
    'password_placeholder' => 'Придумайте ваш пароль',
    'password_placeholder_re' => 'Введите ваш пароль еще раз',
    'remember_me' => 'Запомнить меня',
    'enter' => 'Войти',
    'register' => 'Регистрация',
    'register_btn' => 'Зарегистрироваться',
    'sing_in_social' => 'Войти с помощью соц. сети',
    'sign_up_social' => 'Регистрация с помощью соц. сети',
    'forgot_password' => 'Забыли пароль?',
    'sign_up' => 'Зарегистрировать аккаунт',
    'i_have_account' => 'У меня уже есть аккаунт',
    'password_reset' => 'Восстановление доступа',
    'send_reset_password_link' => 'Восстановить пароль',
    'signin_up' => 'Регистрация',
    'or' => 'или',

    'confirm_text' => 'Подтвердите свой аккаунт, проверьте свой почтовый ящик.',
    'confirm_text_2' => 'Если вы не получили письмо',
    'confirm_text_3' => 'нажмите сюда',
    'confirm_text_4' => 'и мы отправим вам новое.',
];
