<?php

return [
    'h1' => 'Создание <span class="text-success">промо</span><br>видеоролика',
    'h2' => 'Промо видео <span class="text-success">ТОП-5</span> преимуществ',
    'strength1_title' => 'Низкая цена',
    'strength1_desc'  => 'Несмотря на сложную работу, производство анимационных 2D роликов стоит не дорого. Благодаря нашей собственной команде художников, и моушэн дизайнеров, мы можем нарисовать очень красивые иллюстрации.',
    'strength2_title' => 'Неограниченная фантазия',
    'strength2_desc'  => 'Креаторы могут придумать любую идею. Мы можем выстроить сценарий для любого вида деятельности, и обрисовать всё что будет интересно вашим клиентам.',
    'strength3_title' => 'Качественное промо',
    'strength3_desc'  => 'Сюжет мы выстраиваем такими образом, что зрителю захочется посмотреть ролик от начала и до конца.',
    'strength4_title' => 'Дикторы-звезды',
    'strength4_desc'  => 'В нашей команде дикторы актеры дубляжей таких фильмов как «Аватар», «Властелин Колец» и «Игра Престолов»',
    'strength5_title' => 'Готовые видео',
    'strength5_desc'  => 'У нас видео библиотека из 44 000 000 лицензионного материала. Мы сможем сделать монтаж, под любую вашу задачу. А смотреться будет – очень профессионально.',
];