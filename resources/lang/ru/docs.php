<?php

return [
    'terms' => 'Я согласен на обработку <a href="/docs/Terms.pdf" target="_blank">персональных данных</a>',
    'privacy_policy' => 'Я согласен с <a href="/docs/Privacy policy.pdf" target="_blank">политикой конфиденциальности</a>'
];