<?php
return [
    'format_1'      => 'Сторис (Instagram или Facebook - 15 секунд)',
    'format_2'      => 'Лента (Instagram или Facebook - 15 секунд)',
    'format_3'      => 'YouTube (30 секунд)',
    'format_4'      => 'Для веб-сайта (30-60 секунд)',

    'video_style_1' => 'Одноразовое предложение',
    'video_style_2' => 'Презентация продукта',

    'congrats_h1'   => 'Опрос пройден',

    'thanks_text'   => 'Спасибо. Это было увлекательно! Мы начнем производство сценария.<br>О готовности сценария мы вам сообщим по e-mail!',

    'fill_form'     => 'Заполните форму ниже, чтобы зарегистрироваться в системе.',
];