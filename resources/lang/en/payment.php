<?php

return [
    'title'   => 'Purchase a video',
    'content' => 'To buy a video and download it without a watermark, send an email to <a href="mailto:admin@video-3d.com">admin@video-3d.com</a> and we will send it to you.'
];