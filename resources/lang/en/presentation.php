<?php

return [
    'h1' => 'VIDEO PRESENTATION',
    'h2' => 'Presentation video  <span class="text-success">TOP 5</span> strengths',
    'strength1_title' => 'Low cost',
    'strength1_desc'  => 'In despite of the difficult work, the production of animated 2D videos  - not expensive.  Thanks to our own team of artists and Motion designers, who can draw very beautiful illustrations.',
    'strength2_title' => 'Professional Narrator',
    'strength2_desc'  => 'We record professional narrator (speaker) in an acoustic studio.  After we process the voice till it sounds great.',
    'strength3_title' => 'Quality Promo',
    'strength3_desc'  => 'We build the plot in such a way that the viewer wants to watch the video from the beginning till the end.',
    'strength4_title' => 'Unlimited Fantasy',
    'strength4_desc'  => 'Creators can come up with any idea. We can build a script for any type of agency, and outline everything that will be interesting to your customers.',
    'strength5_title' => 'Putting the best together',
    'strength5_desc'  => 'To design high-quality video presentations, we will creating scenes in 2D and 3D, and we will accompany live shots from our library. High-quality animation, presentable modeling in 3D, combine this with live frames - and you will have cool presentation material.',
    'strength6_title' => 'Pre-recorded videos',
    'strength6_desc'  => 'We have a video library within 44,000,000 licensed material. We can do the editing for any of your tasks. And it will look very professional.',
];