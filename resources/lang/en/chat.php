<?php
return [
    'h1' => 'Chat with director',
    'hello' => 'Hi',
    'desc' => 'Here you can ask us any question or make adjustments to the work on your video.'
];