<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login' => 'Sign in',
    'email_placeholder' => 'Enter your email',
    'enter_password_placeholder' => 'Enter your password',
    'password_placeholder' => 'Enter your password',
    'password_placeholder_re' => 'Confirm your password',
    'remember_me' => 'Remember me',
    'enter' => 'Sign in',
    'register' => 'Sign up',
    'sing_in_social' => 'Sign in using social',
    'sign_up_social' => 'Sign up using social',
    'forgot_password' => 'Forgot password?',
    'sign_up' => 'Sign up',
    'i_have_account' => 'I already have an account',
    'password_reset' => 'Password reset',
    'send_reset_password_link' => 'Restore password',
    'register_btn' => 'Sign up',
    'signin_up' => 'Sign up',
    'or' => 'or',

    'confirm_text' => 'Verify your account, check your inbox.',
    'confirm_text_2' => 'If you have not received the letter',
    'confirm_text_3' => 'click this link',
    'confirm_text_4' => 'and we will send you a new one.',

];
