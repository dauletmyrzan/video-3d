<?php

return [
    'send'       => 'Send',
    'attach'     => 'Attach file',
    'back'       => 'Back',
    'make_video' => 'Make a video',
    'fill_brief' => 'Fill the brief',
    'get_video'  => 'Get Video',
    'upload'     => 'Upload',
    'next'       => 'Next',
];