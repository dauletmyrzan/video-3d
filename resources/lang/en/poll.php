<?php
return [
    'format_1'      => 'Story (Instagram or Facebook - 15 seconds)',
    'format_2'      => 'Feed (Instagram or Facebook - 15 seconds)',
    'format_3'      => 'YouTube (30 seconds)',
    'format_4'      => 'For your website (30-60 seconds)',

    'video_style_1' => 'One time offer',
    'video_style_2' => 'Product presentation',

    'congrats_h1'   => 'Congratulations!',

    'thanks_text'   => 'Thanks, it was exciting! We will start the production of your video. <br>
We will inform you about the readiness of the video by e-mail!',
    'fill_form' => 'Fill out the form below to register with the system.',
];