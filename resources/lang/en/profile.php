<?php
return [
    'account' => 'My account',
    'h1' => 'Profile',
    'set_photo' => 'Change avatar',
    'delete_photo' => 'Remove avatar',
    'personal_data' => 'Personal data',
    'change_password' => 'Change password',
    'sign_out' => 'Sign out',

];