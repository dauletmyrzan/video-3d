<?php

return [
    'terms' => 'I agree to the <a href="/docs/Terms.pdf" target="_blank">terms</a> of application',
    'privacy_policy' => 'I agree to the <a href="/docs/Privacy policy.pdf" target="_blank">Privacy policy</a>'
];