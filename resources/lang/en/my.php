<?php
return [
    'h1'         => 'Profile',
    'h2'         => 'Для вас мы делаем лучший сценарий',
    'demo_title_1' => 'A demo of your video will be ready in 3 hours',
    'demo_title_2' => 'Now you can download and watch your video',
];