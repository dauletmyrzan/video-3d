<?php

return [
    'h1'   => 'About Video-3D',
    'body' => 'We are a professional team of motion designers. We develop cool videos for your business.<br>We specialize in:
<ul style="list-style-type:none;">
    <li>- Advertising in the feed</li>
    <li>- Advertising in stories</li>
    <li>- Advertising for placement on your web sites</li>
    <li>- YouTube Advertising</li>
</ul>
<p>Get the video - pay later!</p>
<p>We have two graphic stations, a creative team of 11 people, and most
importantly, a love of creating commercials.</p>'
];