<?php

return [
    'h1' => '3D Video<br>animation',
    'h2' => '3D Animation Video Top 5 strengths',
    'strength1_title' => 'Presentability',
    'strength1_desc'  => '3D videos look very stand. Our artists draw beautiful art concepts, then create a 3D motion design for the video.',
    'strength2_title' => 'Professional Narrator',
    'strength2_desc'  => 'We record professional narrator (speaker) in an acoustic studio. After we process the voice till it sounds great',
    'strength3_title' => 'Call attention',
    'strength3_desc'  => 'Animation is much better at attracting attention than shooting with a camera.',
    'strength4_title' => 'Unlimited Fantasy',
    'strength4_desc'  => 'Creators can come up with any idea. We can build a script for any type of agency, and outline everything that will be interesting to your customers.',
    'strength5_title' => 'High-quality animation',
    'strength5_desc'  => 'We spend 145 hours to ensure that the animation of parts, characters, products will be as smooth as it possible. A nice story, along with an exciting picture - it is a key to a successful video.',
];