<?php

return [
    'home' => 'Home',
    'portfolio' => 'Portfolio',
    'reviews' => 'Reviews',
    'my_scripts' => 'My scripts',
    'chat' => 'Chat with director',
    'my_videos' => 'My videos',
    'how_it_works' => 'How it works?',
    'video_types' => 'Video styles',
    'about' => 'About',
    'help' => 'Help',
];