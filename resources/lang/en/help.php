<?php

return [
    'h1' => 'FAQ',
    'h2' => 'Do you have any questions?',
    'form_text' => 'Ask your question to our support team and we will answer it shortly. The answer will come to your mail.'
];