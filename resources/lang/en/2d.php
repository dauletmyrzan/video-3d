<?php

return [
    'h1' => "<span style='color:#ffcb4d;'>2D</span> ANIMATION<br> VIDEO",
    'h2' => "2D Animation Video <span class='text-success'>TOP-5</span> strengths",
    'strength1_title' => 'Low cost',
    'strength1_desc'  => 'In despite of the difficult work, the production of animated 2D videos  - not expensive. Thanks to our own team of artists and Motion designers, who can draw very beautiful illustrations.',
    'strength2_title' => 'Unlimited Fantasy',
    'strength2_desc'  => 'Creators can come up with any idea. We can build a script for any type of agency, and outline everything that will be interesting to your customers.',
    'strength3_title' => 'High-quality animation',
    'strength3_desc'  => 'We spend 145 hours to ensure that the animation of parts, characters, products will be as smooth as it possible. A nice story, along with an exciting picture - it is a key to a successful video.',
    'strength4_title' => 'Professional Narrator',
    'strength4_desc'  => 'We record professional narrator (speaker) in an acoustic studio. After we process the voice till it sounds great.',
    'strength5_title' => 'Call attention',
    'strength5_desc'  => 'Animation is much better at attracting attention than shooting with a camera.',
    'see_more'        => '<span class="text-success">See more </span>of our works',
    'special_title'   => 'Special promotion',
    'special_desc'    => 'Fill the brief bellow and get a demo video <span class="text-orange">FOR FREE</span>'
];