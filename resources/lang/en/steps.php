<?php

return [
    'step' => 'Step',
    '1' => 'Step 1',
    '2' => 'Step 2',
    '3' => 'Step 3',
    '4' => 'Step 4',
    '5' => 'Step 5',
    'title_about' => 'Take this survey and get your video',
    'title_start' => 'Watch video instruction before starting',

    '1_title' => 'You pass a small interrogation',
    '2_title' => 'We are designing a business video',
    '3_title' => 'The video is ready. You approve it, or tell us to make edits.',

    '1_desc' => 'You answer for 8 questions and we can make for you business video',
    '2_desc' => 'We produce a super video based on the answers you gave in the interrogation.',
    '3_desc' => 'If something doesn’t suit you in the finished video, you can easily change it.<br>
    After that, pay for the video in order to get a license and video without a watermark.',

    'first_desc'  => 'Watch this video before you start answering to questions. This video is important, because we want
    to create the best video for your business.',
    'second_desc' => 'One time offer - video 10-15 seconds duration, with last minute offer.<br>
    Product presentation – video 20-30 seconds duration , explains point of the product or service.',
    'third_desc'  => 'Please write in detail what kind of texts should be in the video.',
    'fourth_desc' => 'We need this make a video within your capabilities. The higher the price, the higher opportunities we have for creativity.',
    'six_desc'    => 'Try to find similar videos on YouTube and send us the link',
    'seven_desc'  => 'Attach your logo, photos of your product or services. We will make video as part of your corporate identity',
    'video_watched' => 'I watched the video. Now I know how to get the video',
];