<?php
return [
    'h1' => 'Choose your video style, which will suits to your business',
    'text' => '2D Animation Video – animated video that will be explainer of your product. 
3D Animation Video – designed hard models with, to focus on the premium of your products.  
Presentation Video – long video where we combined video from Pre-recorded video stocks , animation, 2d design and и 3d modeling. 
Promo Video – short video created special as one time offer for advertising in social media.',
    'first' => '2D Animation Video',
    'second' => '3D Animation Video',
    'third' => 'Presentation Video',
    'fourth' => 'Promo Video'
];