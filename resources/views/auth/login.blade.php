@extends('layouts.app', ['bodyClass' => 'bg'])

@section('content')
<div class="container min-vh-md-70">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <form method="POST" action="{{ route('login') }}" style="width: 90%; margin: 0 auto;">
                @csrf
                <div class="h3 mb-3 font-weight-bold text-center text-green">{{ __('auth.enter') }}</div>
                <div class="form-group row">
                    <input id="email" type="email" class="text-center form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="{{ __('auth.email_placeholder') }}">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group row">
                    <input id="password" type="password" class="text-center form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="{{ __('auth.enter_password_placeholder') }}">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group row">
                    <button type="submit" class="btn btn-success w-100">{{ __('auth.login') }}</button>
                </div>
                <div class="form-group d-flex row">
                    <div class="flex-fill text-left">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="form-check-label" for="remember">
                                {{ __('auth.remember_me') }}
                            </label>
                        </div>
                    </div>
                    <div class="text-right flex-fill">
                        @if (Route::has('password.request'))
                            <a class="text-success" href="{{ route('password.request', ['lang' => app()->getLocale()]) }}">
                                <u>{{ __('auth.forgot_password') }}</u>
                            </a>
                        @endif
                    </div>
                </div>
                <div class="form-group row mb-0 text-center">
                    <a class="mx-auto text-dark d-block dark-link" href="{{ route('register', ['lang' => app()->getLocale()]) }}">{{ __('auth.sign_up') }}</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
