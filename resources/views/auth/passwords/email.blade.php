@extends('layouts.app', ['bodyClass' => 'bg'])

@section('content')
<div class="container min-height-500">
    <div class="row justify-content-center mt-0 mt-md-5">
        <div class="col-md-4 mt-0 mt-md-5">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <form method="POST" action="{{ route('password.email', app()->getLocale()) }}">
                @csrf
                <div class="form-group row">
                    <label class="h5 mb-3 font-weight-normal d-block mx-auto" for="email">{{ __('auth.password_reset') }}</label>
                    <input id="email" type="email" class="text-center form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="{{ __('auth.email_placeholder') }}">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group row mb-0">
                    <button type="submit" class="btn btn-success w-100">
                        {{ __('auth.send_reset_password_link') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
