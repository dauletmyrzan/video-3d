@extends('layouts.app', ['footerClass' => 'sticky'])

@section('content')
<div class="container min-vh-md-50 mt-md-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mt-md-5">
                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('auth.confirm_text') }}
                    {{ __('auth.confirm_text_2') }}, <a href="{{ route('verification.resend') }}">{{ __('auth.confirm_text_3') }}</a>
                    {{ __('auth.confirm_text_4') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
