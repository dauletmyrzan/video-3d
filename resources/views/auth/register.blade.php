@extends('layouts.app', ['bodyClass' => 'bg'])

@section('additional-css')
    {!! htmlScriptTagJsApi([
            'action' => 'homepage'
    ]) !!}
@endsection

@section('content')
<div class="container min-height-500">
    <div class="row justify-content-center">
        <div class="col-md-4">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul class="m-0 p-0" style="list-style-type:none;">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="POST" action="{{ route('register') }}" style="width: 90%; margin: 0 auto;">
                @csrf
                <div class="h3 mb-3 font-weight-bold text-center text-green">{{ __('auth.register') }}</div>
                <div class="form-group row">
                    <input id="email" type="email" class="text-center form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="{{ __('auth.email_placeholder') }}">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group row">
                    <input id="password" type="password" class="text-center form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="{{ __('auth.password_placeholder') }}">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group row">
                    <input id="password-confirm" type="password" class="text-center form-control" name="password_confirmation" required placeholder="{{ __('auth.password_placeholder_re') }}">
                </div>
                {!! htmlFormSnippet() !!}
                <div class="form-group row mb-0 mt-3">
                    <button type="submit" class="btn btn-success w-100">
                        {{ __('auth.register_btn') }}
                    </button>
                </div>
                <div class="form-group row mt-3 mb-0 text-center">
                    <a class="mx-auto text-dark d-block dark-link" href="{{ route('login', ['lang' => app()->getLocale()]) }}">{{ __('auth.i_have_account') }}</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection