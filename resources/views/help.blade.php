@extends('layouts.app', ['bodyClass' => 'bg'])

@section('additional-css')
@endsection

@section('content')
    <div class="container mt-5 min-height-500 regular-page min-vh-md-60">
        <h1 class="h2">{{ __('help.h2') }}<hr class="w-15"></h1>
        <div class="row mt-md-5">
            @foreach($questions as $key => $question)
                <div class="col-md-5">
                    <div class="question card border-0 shadow mb-5">
                        <div class="card-body px-md-5">
                            <div class="h5 m-0 font-weight-bold cursor-pointer" data-toggle="collapse" href="#faq{{ $key }}">{{ $question->question }}
                                <img src="{{ asset('img/down-arrow-1.svg') }}" class="float-right mt-1" width="20">
                            </div>
                            <div class="collapse" id="faq{{ $key }}">
                                <div class="mt-3">
                                    {{ $question->answer }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="mt-0">
            <p class="w-md-50">{{ __('help.form_text') }}</p>
            @if(session('message'))
                <div class="alert alert-success">{{ session('message') }}</div>
            @endif
        </div>
        <div class="row mt-5">
            <div class="col-md-1 mb-2">
                <img src="{{ asset('storage/' . \Auth::user()->img) }}" class="rounded-circle shadow" width="50">
            </div>
            <div class="col-md-10">
                <form action="/ask_question" class="d-block w-100" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="with-arrow">
                        <div class="attach cursor-pointer mb-3"><u>{{ __('buttons.attach') }}</u> <img class="ml-1" src="{{ asset('img/attach.svg') }}"
                                                                                                width="20"></div>
                        <input type="file" style="display:none;" name="file">
                        <textarea name="message" required class="p-4 mb-4 form-control border-0 shadow" placeholder="{{ __('placeholders.enter_question') }}" style="resize:none;min-height: 130px;" required></textarea>
                        <div class="attached mb-3 text-primary" style="display:none;"></div>
                    </div>
                    <button class="btn btn-success px-5">{{ __('buttons.send') }}</button>
                </form>
            </div>
        </div>
        <div class="min-vh-md-10"></div>
    </div>
@endsection

@section('page-scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.collapse').on('show.bs.collapse', function(){
                $(this).parent().find('img').attr('src', 'img/up-arrow-1.svg');
            });
            $('.collapse').on('hide.bs.collapse', function(){
                $(this).parent().find('img').attr('src', 'img/down-arrow-1.svg');
            });
            $('.attach').on('click', function(){
                $(this).parent().find('input[type=file]').trigger('click');
            });
            $(':file').on("change", function(){
                let attached = '';
                if(this.files.length > 3){
                    alert('Вы можете загрузить максимум 3 файла. Если их у вас больше, отправьте архивом.');
                    return false;
                }
                for(let i = 0; i < this.files.length; i++){
                    let file = this.files[i];
                    if(file.size > 10*1024*1024){
                        alert('Размер файла не должен превышать 10 Мб.');
                        return false;
                    }
                    attached += '<span class="d-block">' + file.name + '</span>';
                }
                $('.attached').html(attached).show();
            });
        });
    </script>
@endsection