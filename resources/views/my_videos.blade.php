@extends('layouts.app', ['bodyClass' => 'bg'])

@section('additional-css')
@endsection

@section('content')
    <div class="container mt-5 min-height-500 regular-page min-vh-md-80">
        <h1 class="h2">{{ __('my_videos.h1') }}<hr class="w-15"></h1>
        <div class="row mt-md-5 text-center">
            @foreach($videos as $video)
                <div class="col-md-3 my-video">
                    <div class="h5 my-3 font-weight-bold">{{ $video->title }}</div>
                    <div class="position-relative mb-3">
                        @if($video->status_id == 1)
                            <div class="in-process"><span>{{ __('main.in_process') }}</span></div>
                        @endif
                        <img src="{{ asset('/img/my_videos.jpg') }}" alt="{{ __('my_videos.first') }}" width="100%" class="shadow rounded">
                    </div>
                    @if($video->filename && $video->status_id == 2)
                    <a href="{{ asset('storage/' . $video->filename) }}" download class="btn btn-success d-block w-90 mx-auto mb-3">{{ __('main.download') }} <img src="{{ asset('img/download.svg') }}"
                                                                                                             width="20"></a>
                    @else
                        <button class="btn btn-success d-block w-90 mx-auto mb-3" disabled>{{ __('statuses.' . $video->status_id) }}</button>
                    @endif
                    @if(!$video->approved && $video->status_id == 2)
                        <button data-video_id="{{ $video->id }}" class="btn btn-outline-success d-block w-90 py-3 mx-auto mb-2 approve-video"><b>{{ __('main.approve') }} <img src="{{ asset('img/check.svg') }}"
                                                                                                                                width="20"></b></button>
                        <a href="/{{ app()->getLocale() }}/my-videos/{{ $video->id }}/feedback" class="text-dark"><u>{{ __('main.edit') }}</u></a>
                    @elseif($video->status_id == 2)
                        <div class="text-success-darker">Your video is ready!</div>
                    @endif
                </div>
            @endforeach
        </div>
        @if($videos->count() == 0)
            <div class="h4">You don't have any videos yet.</div>
        @endif
    </div>
@endsection

@section('page-scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.btn-outline-success').mouseenter(function(){
                $(this).find('img').attr('src', 'img/check-white.svg');
            }).mouseleave(function(){
                $(this).find('img').attr('src', 'img/check.svg');
            });
            $(".approve-video").on("click", function(){
                let id = $(this).attr("data-video_id");
                $.ajax({
                    url: "/approve_video",
                    type: "POST",
                    data: {
                        _token: $("input[name=_token]").val(),
                        id: id
                    },
                    dataType: "json",
                    success(response){
                        alert(response['message']);
                        window.location.reload();
                    }
                });
            });
        });
    </script>
@endsection