@extends('layouts.app', ['bodyClass' => 'bg'])

@section('content')
    <div class="container mt-5 min-height-500 regular-page min-vh-md-60">
        <h1 class="h2">Обратная связь по сценарию «{{ $script->title }}»<hr class="w-15"></h1>
        <div class="row mt-md-5">
            <div class="col-md-8" style="max-height:400px;overflow-y:auto;" id="scroll">
                @foreach($script->comments as $comment)
                    <div class="card shadow-sm mb-4">
                        <div class="card-body {{ \Auth::user()->id == $comment->author_id ? 'text-right' : '' }}">
                            <span class="text-muted font-weight-bold">{{ \Auth::user()->id == $comment->author_id ? 'Вы:' : $comment->author->name }}</span>
                            <span class="text-muted font-size-10">({{ $comment->created_at }})</span>
                            <span class="d-block">{{ $comment->message }}</span>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <form action="/script_feedback" method="POST">
                    @csrf
                    <input type="hidden" name="script_id" value="{{ $script->id }}">
                    <div class="form-group">
                        <label>Ваше сообщение:</label>
                        <textarea class="form-control shadow border-0 p-4" style="resize:none;" rows="5" name="message" required></textarea>
                    </div>
                    <button class="btn btn-success w-40 mr-2">Отправить</button>
                    <a href="/{{ app()->getLocale() }}/my-scripts" class="btn btn-light px-4">Назад</a>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <script type="text/javascript">
        $(function(){
            var wtf = $('#scroll');
            var height = wtf[0].scrollHeight;
            wtf.scrollTop(height);
        });
    </script>
@endsection