@extends('layouts.app')

@php
    $poll        = session('poll');
    $pollAnswers = session('pollAnswers');
    $id          = isset($question) && $question ? $question->id : 0;
    $code        = isset($question) && $question ? $question->code : '';
    $answer      = $pollAnswers && isset($pollAnswers[$code]) ? $pollAnswers[$code]->answer : '';
@endphp

@section('content')
    <div class="container-fluid regular-page mt-5 profile min-vh-100">
        <h1 class="h2 d-inline-block mb-3">
            @if($id > 0 && $id < 7)
                {{ __('steps.step') }} {{ $id }}<hr class="w-70">
            @elseif($code == 'about')
                {{ __('steps.title_about') }}
            @elseif($code == 'start')
                {{ __('steps.title_start') }}
            @endif
        </h1>
        <div class="row mt-4 mb-5">
            <div class="col-md-3">
                <img src="{{ asset('img/gifs/' . ($id+1) . '.gif') }}" width="100%">
            </div>
            <div class="col-md-6">
                <div class="card shadow border-0" style="border-radius:10px;">
                    <div class="card-body px-4">
                        {{ app()->getLocale() == 'en' ? $question->question : $question->question_ru }}
                        <form action="/post-steps/{{ $code }}" method="POST" class="mb-5">
                            @csrf
                            <div class="py-3 my-3">
                                @if($code == '')
                                    <p style="font-size:11pt;">{!! __('steps.first_desc') !!}</p>
                                    <div class="shadow-sm" style="background-color:#222;height:300px;"></div>
                                @elseif($code == 'format')
                                    <div class="form-group">
                                        <input type="radio" required name="answer" {{ $answer == __('poll.format_1') ? 'checked' : '' }} value="{{ __('poll.format_1') }}" id="option_1"> <label for="option_1">{{ __('poll.format_1') }}</label><br>
                                        <input type="radio" required name="answer" {{ $answer == __('poll.format_2') ? 'checked' : '' }} value="{{ __('poll.format_2') }}" id="option_2"> <label for="option_2">{{ __('poll.format_2') }}</label><br>
                                        <input type="radio" required name="answer" {{ $answer == __('poll.format_3') ? 'checked' : '' }} value="{{ __('poll.format_3') }}" id="option_3"> <label for="option_3">{{ __('poll.format_3') }}</label><br>
                                        <input type="radio" required name="answer" {{ $answer == __('poll.format_4') ? 'checked' : '' }} value="{{ __('poll.format_4') }}" id="option_4"> <label for="option_4">{{ __('poll.format_4') }}</label><br>
                                    </div>
                                @elseif($code == 'videostyle')
                                    <div class="form-group">
                                        <input type="radio" required name="answer" {{ $answer == __('poll.video_style_1') ? 'checked' : '' }} value="{{ __('poll.video_style_1') }}" id="option_1"> <label for="option_1">{{ __('poll.video_style_1') }}</label><br>
                                        <input type="radio" required name="answer" {{ $answer == __('poll.video_style_2') ? 'checked' : '' }} value="{{ __('poll.video_style_2') }}" id="option_2"> <label for="option_2">{{ __('poll.video_style_2') }}</label><br>
                                    </div>
                                    <div class="step-desc">
                                        <i class="fas fa-info-circle"></i>
                                        {!! __('steps.second_desc') !!}
                                    </div>
                                @elseif($code == 'text' || $code == 'about')
                                    <div class="form-group">
                                        <textarea class="form-control step-textarea mb-4" name="answer" required placeholder="..." required>{{ $answer }}</textarea>
                                    </div>
                                    <div class="step-desc">
                                        <i class="fas fa-info-circle"></i>
                                        {{ __('steps.third_desc') }}
                                    </div>
                                @elseif($code == 'minimal_budget' || $code == 'maximum_budget')
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">$</span>
                                        </div>
                                        <input type="number" min='{{ $code == 'maximum_budget' && isset($pollAnswers['minimal_budget']) ? $pollAnswers['minimal_budget']->answer : '' }}' name="answer" class="form-control form-control-lg" aria-describedby="basic-addon1" required value="{{ $answer }}">
                                    </div>
                                    <div class="step-desc">
                                        <i class="fas fa-info-circle"></i>
                                        {!! __('steps.fourth_desc') !!}
                                    </div>
                                @elseif($code == 'start')
                                    <iframe src="https://player.vimeo.com/video/431160604?autoplay=1" width="100%" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                @else
                                    @if($code == 'file')
                                        <progress style="display:none;margin:20px auto;"></progress>
                                        <div class="form-group">
                                            <input type="file" class="form-control" name="file" style="height:auto;">
                                            @if($poll->filename)
                                            <p class="mt-2" style="font-size:10pt;">Your uploaded file: <u>{{ $poll->filename }}</u></p>
                                            @endif
                                        </div>
                                        <div class="step-desc">
                                            <i class="fas fa-info-circle"></i>
                                            {!! __('steps.seven_desc') !!}
                                        </div>
                                    @else
                                        <div class="form-group">
                                            <input type="url" class="form-control mb-4" id="url" name="answer" placeholder="https://www.youtube.com/" required value="{{ $answer }}">
                                        </div>
                                        <div class="step-desc"><i class="fas fa-info-circle"></i> {!! __('steps.six_desc') !!}</div>
                                    @endif
                                @endif
                                @if($code == 'about')
                                    <div class="form-group text-center mt-3">
                                        <input type="checkbox" id="info_gathering">
                                        <label for="info_gathering">{!! __('docs.terms') !!}</label>
                                    </div>
                                @endif
                            </div>
                            <div class="text-center mb-4">
                                @if($id > 0 && $code != 'start')
                                    <a href="/{{ app()->getLocale() }}/steps/{{ $prevCode }}" class="btn btn-light border mr-3 px-5"><i class="fas fa-arrow-left"></i>
                                        {{ __('buttons.back') }}</a>
                                @endif
                                @if($id < 8 && $code != 'start')
                                    <button class="btn btn-success px-5 next-btn">{{ __('buttons.next') }} <i class="fas fa-arrow-right"></i></button>
                                @elseif($code == 'start')
                                    <input type="checkbox" id="video_watched"> <label for="video_watched">{{ __('steps.video_watched') }}</label>
                                    <button class="btn btn-success px-5 next-btn mt-3" id="next_btn" disabled>{{ __('buttons.next') }} <i class="fas fa-arrow-right"></i></button>
                                @else
                                    <button type="button" id="upload" class="btn btn-primary px-4 mr-2" disabled>{{ __('buttons.upload') }}</button>
                                    <input type="submit" name="finish" class="btn btn-success px-5" value="Finish">
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <script>
        $(document).ready(function(){
            let progressFunction = function(){
                $("progress").css("display", "block");
            };
            $("input[name=file]").on("change", function(){
                if ($(this).val().length) {
                    $("#upload").removeAttr("disabled");
                } else {
                    $("#upload").attr("disabled", "disabled");
                }
            });
            $("#video_watched").on("change", function () {
                $("#next_btn").prop("disabled", !$(this).prop("checked"));
            });
            $("#info_gathering").on("change", function () {
                $(".next-btn").prop("disabled", !$(this).prop("checked"));
            });
            if($("input[type=checkbox]").length) {
                $(".next-btn").prop("disabled", true);
            }
            $('#upload').on("click", function(){
                let formData = new FormData();
                formData.append('_token', $("input[name=_token]").val());
                formData.append('file', $("input[name=file]")[0].files[0]);
                $.ajax({
                    url: '/steps-upload-file',
                    type: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    dataType: 'json',
                    xhr: function() {
                        let myXhr = $.ajaxSettings.xhr();
                        if(myXhr.upload){
                            myXhr.upload.addEventListener('progress', progressFunction, false);
                        }
                        return myXhr;
                    },
                    success: function(response){
                        alert(response['message']);
                        $("progress").css("display", "none");
                    },
                    error: function () {
                        $("progress").css("display", "none");
                    },
                    cache: false,
                });
            });
        });
    </script>
@endsection