<?
    header('Content-type: text/html; charset=UTF-8');
    $months = array(
        1 => 'января',
        2 => 'февраля',
        3 => 'марта',
        4 => 'апреля',
        5 => 'мая',
        6 => 'июня',
        7 => 'июля',
        8 => 'августа',
        9 => 'сентября',
        10 => 'октября',
        11 => 'ноября',
        12 => 'декабря'
    );
    $month_name = $months[date('n')];
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">
        * {
            font-family: dejavu sans;
            font-size: 14px;
            line-height: 14px;
        }
        table {
            margin: 0 0 15px 0;
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
        }
        table td {
            padding: 5px;
        }
        table th {
            padding: 5px;
            font-weight: bold;
        }

        .header {
            margin: 0 0 0 0;
            padding: 0 0 15px 0;
            font-size: 12px;
            text-align: right;
            line-height: 12px;
        }

        /* Реквизиты банка */
        .details td {
            padding: 3px 2px;
            border: 1px solid #000000;
            font-size: 12px;
            line-height: 12px;
            vertical-align: top;
        }

        h1 {
            margin: 0 0 10px 0;
            padding: 10px 0 10px 0;
            border-bottom: 2px solid #000;
            font-weight: bold;
            font-size: 20px;
        }

        /* Поставщик/Покупатель */
        .contract th {
            padding: 3px 0;
            vertical-align: top;
            text-align: left;
            font-size: 13px;
            line-height: 15px;
        }
        .contract td {
            padding: 3px 0;
        }

        /* Наименование товара, работ, услуг */
        .list thead, .list tbody  {
            border: 2px solid #000;
        }
        .list thead th {
            padding: 4px 0;
            border: 1px solid #000;
            vertical-align: middle;
            text-align: center;
        }
        .list tbody td {
            padding: 0 2px;
            border: 1px solid #000;
            vertical-align: middle;
            font-size: 11px;
            line-height: 13px;
        }
        .list tfoot th {
            padding: 3px 2px;
            border: none;
            text-align: right;
        }

        /* Сумма */
        .total {
            margin: 0 0 20px 0;
            padding: 0 0 10px 0;
            border-bottom: 2px solid #000;
        }
        .total p {
            margin: 0;
            padding: 0;
        }

        /* Руководитель, бухгалтер */
        .sign {
            position: relative;
        }
        .sign table {
            width: 60%;
        }
        .sign th {
            padding: 40px 0 0 0;
            text-align: left;
        }
        .sign td {
            padding: 40px 0 0 0;
            border-bottom: 1px solid #000;
            text-align: right;
            font-size: 12px;
        }

        .sign-1 {
            position: absolute;
            left: 149px;
            top: -44px;
        }
        .sign-2 {
            position: absolute;
            left: 149px;
            top: 0;
        }
        .printing {
            position: absolute;
            left: 150px;
            top: -15px;
        }
    </style>
</head>
<body>
<p class="header">
    * Внимание! Оплата данного счета означает согласие с условиями поставки товара.
    Уведомление об оплате обязательно, в противном случае не гарантируется наличие
    товара на складе. Товар отпускается по факту прихода денег на р/с Поставщика,
    самовывозом, при наличии доверенности и документов, удостоверяющих личность.
</p>

<table class="details">
    <tbody>
        <tr>
            <td>
                <b>Бенефициар:</b><br>
                <b>Индивидуальный Предприниматель А. М. Абдуллаев</b><br>
                ИИН: 940601302178
            </td>
            <td>
                <b>ИИК</b><br>
                KZ656017131000068654
            </td>
            <td>
                <b>КБе</b><br>
                9
            </td>
        </tr>
        <tr>
            <td><b>Банк бенефициара:</b><br>АО "Народный Банк Казахстана"</td>
            <td><b>БИК</b><br>HSBKKZKX</td>
            <td><b>Код назначения платежа</b></td>
        </tr>
    </tbody>
</table>

<h1>Счет на оплату № <?=$invoice_number ?> от <?=date("d") . " " . $month_name . " " . date("Y") ?> г.</h1>

<table class="contract">
    <tbody>
        <tr>
            <td width="15%">Поставщик:</td>
            <th width="85%">
                ИИН: 940601302178 Индивидуальный Предприниматель А. М. Абдуллаев, Курмангазы 107
            </th>
        </tr>
        <tr>
            <td>Покупатель:</td>
            <th>
                БИН: <?=$bin?>, <?=$company?>, <?=$address?>
            </th>
        </tr>
        <tr>
            <td>Договор:</td>
            <td><b>Без договора</b></td>
        </tr>
    </tbody>
</table>

<table class="list">
    <thead>
        <tr>
            <th width="5%">№</th>
            <th width="54%">Наименование товара, работ, услуг</th>
            <th width="8%">Кол-во</th>
            <th width="5%">Ед. изм.</th>
            <th width="14%">Цена</th>
            <th width="14%">Сумма</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td align="center"></td>
            <td align="left"></td>
            <td align="right"></td>
            <td align="left"></td>
            <td align="right"></td>
            <td align="right"></td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <th colspan="5">Итого:</th>
            <th></th>
        </tr>
        <tr>
            <th colspan="5">В том числе НДС:</th>
            <th>Без НДС</th>
        </tr>
    </tfoot>
</table>

<div class="sign">
    <img class="sign-1" width="150" src="{{ asset('storage/sign.png') }}">
    <img class="printing" width="200" src="{{ asset('storage/printing.png') }}">
    <table>
        <tbody>
            <tr>
                <th width="30%">Исполнитель</th>
                <th>___________________</th>
            </tr>
        </tbody>
    </table>
</div>
</body>
</html>