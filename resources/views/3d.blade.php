@extends('layouts.app', ['headerClass' => 'dark'])

@section('content')
    <div class="container-fluid regular-page min-vh-md-70 landing-page px-0">
        <section id="three_d_video_top" class="landing-top">
            <div class="container">
                <img src="{{ asset('img/3d-bg.png') }}" width="100%" class="video-2d">
                <h1 class="video-3d-h1" style="top:200px;">{!! __('3d.h1') !!}</h1>
            </div>
        </section>
        <div class="container">
            <h2 class="text-center m-auto">{!! __('3d.h2') !!}</h2>
            <div class="row justify-content-md-center mt-md-5 text-center">
                <div class="col-md-4 mt-md-5">
                    <img src="{{ asset('img/star.png') }}" alt="{{ __('3d.strength1_title') }}" height="75">
                    <div class="mt-md-4 h6 font-weight-bold">{{ __('3d.strength1_title') }}</div>
                    <p class="font-size-11 font-weight-normal">{{ __('3d.strength1_desc') }}</p>
                </div>
                <div class="col-md-4 mt-md-5">
                    <img src="{{ asset('img/mic.png') }}" alt="{{ __('3d.strength2_title') }}" height="75">
                    <div class="mt-md-4 h6 font-weight-bold">{{ __('3d.strength2_title') }}</div>
                    <p class="font-size-11 font-weight-normal">{{ __('3d.strength2_desc') }}</p>
                </div>
                <div class="col-md-4 mt-md-5">
                    <img src="{{ asset('img/heart.png') }}" alt="{{ __('3d.strength3_title') }}" height="75">
                    <div class="mt-md-4 h6 font-weight-bold">{{ __('3d.strength3_title') }}</div>
                    <p class="font-size-11 font-weight-normal">{{ __('3d.strength3_desc') }}</p>
                </div>
                <div class="col-md-4 mt-md-5">
                    <img src="{{ asset('img/bulb.png') }}" alt="{{ __('3d.strength4_title') }}" height="75">
                    <div class="mt-md-4 h6 font-weight-bold">{{ __('3d.strength4_title') }}</div>
                    <p class="font-size-11 font-weight-normal">{{ __('3d.strength4_desc') }}</p>
                </div>
                <div class="col-md-4 mt-md-5">
                    <img src="{{ asset('img/hat.png') }}" alt="{{ __('3d.strength5_title') }}" height="75">
                    <div class="mt-md-4 h6 font-weight-bold">{{ __('3d.strength5_title') }}</div>
                    <p class="font-size-11 font-weight-normal">{{ __('3d.strength5_desc') }}</p>
                </div>
            </div>
        </div>
        <div class="container mt-md-5 pb-4">
            <h2 class="m-auto text-center">{!! __('2d.see_more') !!}</h2>
            <div class="row mt-5">
                <div class="col-md-6 two-d-bottom-img mt-4 bottom-video" data-src="https://www.youtube.com/embed/B0_AwZDpovE">
                    <img src="{{ asset('img/3d-1.png') }}" width="100%" class="shadow bg-white" style="padding:5px;border-radius:10px;">
                    <img src="{{ asset('img/play.png') }}" class="shadow rounded-circle play-btn">
                </div>
                <div class="col-md-6 two-d-bottom-img mt-4 bottom-video" data-src="https://www.youtube.com/embed/B245zne298g">
                    <img src="{{ asset('img/3d-2.png') }}" width="100%" class="shadow bg-white" style="padding:5px;border-radius:10px;">
                    <img src="{{ asset('img/play.png') }}" class="shadow rounded-circle play-btn">
                </div>
            </div>
        </div>
        <section class="bg-green mt-5 py-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 mt-4">
                        <div class="h1 text-orange font-weight-bolder">{{ __('2d.special_title') }}</div>
                        <p>{!! __('2d.special_desc') !!}</p>
                        <div class="w-md-50 text-center">
                            <a href="/{{ app()->getLocale() }}/steps/start" class="btn btn-white w-100 d-block mb-3">{{ __('buttons.fill_brief') }}</a>
                        </div>
                    </div>
                    <div class="col-md-6 mt-4">
                        <img src="{{ asset('img/mac-2.png') }}" width="100%">
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <iframe width="100%" height="500" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $(".bottom-video").on("click", function(){
                let src = $(this).data("src");
                let modal = $("#videoModal");
                modal.find("iframe").attr("src", src);
                modal.modal('show');
            });
        });
    </script>
@endsection