@extends('layouts.app', ['bodyClass' => 'bg', 'footerSvgColor' => '#f3f8fc'])

@section('content')
    <div class="container min-height-500">
        <img src="{{ asset('img/2d-bg.png') }}" alt="2D Animation Video" id="bg_2d">
        <img src="{{ asset('img/rabbit.png') }}" width="270" id="">
    </div>
@endsection