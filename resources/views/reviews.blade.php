@extends('layouts.app', ['bodyClass' => 'bg'])

@section('additional-css')
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slick-theme.css') }}">
    <style>
        .slick-left{
            left: -50px;
        }
        .slick-right{
            right: -50px;
        }
    </style>
@endsection

@section('content')
    <div class="container regular-page mt-5 min-vh-100">
        <h1 class="h2 d-inline-block">Отзывы<hr class="w-50"></h1>
        <div class="row mt-5 justify-content-center">
            <div class="col-md-10">
                <div class="review-slider">
                    <div class="review-item">
                        <div class="d-flex">
                            <div class="fill-flex px-2">
                                <img src="{{ asset('storage/review.jpg') }}" alt="video3d отзывы" class="rounded">
                            </div>
                            <div class="flex-fill px-2">
                                <div class="h2">"Что-то главное из отзыва"</div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </div>
                        </div>
                    </div>
                    <div class="review-item">
                        <div class="d-flex">
                            <div class="flex-fill px-2">
                                <img src="{{ asset('storage/review.jpg') }}" alt="video3d отзывы" class="rounded">
                            </div>
                            <div class="flex-fill px-2">
                                <div class="h2">"Что-то главное из отзыва"</div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <script type="text/javascript" src="{{ asset('js/slick.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.review-slider').slick({
                dots: true,
                prevArrow: '<img src="img/left-arrow-white.png" width="42" class="slick-arrow slick-left shadow-lg rounded-circle">',
                nextArrow: '<img src="img/right-arrow-white.png" width="42" class="slick-arrow slick-right shadow-lg rounded-circle">'
            });
        });
    </script>
@endsection