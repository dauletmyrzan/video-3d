@extends('layouts.admin', ['headerClass' => 'py-6', 'pageTitle' => 'Вопросы от клиентов'])

@section('header-content')
    <a href="{{ url()->previous() }}" class="btn btn-white m-0"><i class="fas fa-angle-left"></i> Назад</a>
@endsection

@section('content')
    <div class="row my-3">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th>#</th>
                                <th>Вопрос</th>
                                <th>Автор</th>
                                <th></th>
                                <th>Дата вопроса</th>
                            </thead>
                            <tbody>
                            @foreach($questions as $key => $q)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $q->message }}</td>
                                    <td>{{ $q->author->name ?? '' }} {{ $q->author->lastname ?? '' }} ({{ $q->author->email }})</td>
                                    <td>
                                        @if($q->filename)
                                            <a href="{{ asset('storage/' . $q->filename) }}" target="_blank"><i class="fas fa-paperclip"></i> Прикрепленный файл</a>
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>{{ $q->created_at }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $questions->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection