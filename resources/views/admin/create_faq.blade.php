@extends('layouts.admin', ['headerClass' => 'py-6', 'pageTitle' => 'Изменение FAQ'])

@section('header-content')
    <a href="{{ url()->previous() }}" class="btn btn-white m-0"><i class="fas fa-angle-left"></i> Назад</a>
@endsection

@section('content')
    <div class="row my-3">
        <div class="col-md-6">
            <div class="card shadow">
                <div class="card-body">
                    <form action="{{ route('store_faq') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="question">Вопрос</label>
                            <input type="text" name="question" required class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="answer">Ответ</label>
                            <textarea name="answer" required class="form-control" style="resize:none;" rows="3"></textarea>
                        </div>
                        <button class="btn btn-success">Добавить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection