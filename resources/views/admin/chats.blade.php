@extends('layouts.admin', ['headerClass' => 'py-6', 'pageTitle' => 'Чаты'])

@section('header-content')
    <a href="{{ url()->previous() }}" class="btn btn-white m-0"><i class="fas fa-angle-left"></i> Назад</a>
@endsection

@section('content')
    <div class="row my-3">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>Клиент</th>
                            <th>Режиссер</th>
                            <th>Дата</th>
                        </thead>
                        @foreach($chats as $key => $chat)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $chat->author->name }} {{ $chat->author->lastname }} ({{ $chat->author->email }})</td>
                            <td>{{ $chat->director ?? '-' }}</td>
                            <td>{{ $chat->created_at }}</td>
                            <td><a href="/admin/chats/{{ $chat->id }}" class="btn btn-success btn-sm">Открыть</a></td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
