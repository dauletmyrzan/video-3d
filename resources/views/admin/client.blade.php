@extends('layouts.admin', ['headerClass' => 'py-6', 'pageTitle' => 'Данные клиента'])

@section('additional-css')
    <style>
        table td:nth-of-type(odd){
            width: 40%;
            text-align: right;
        }
        table td:nth-of-type(even){
            font-weight: bold;
        }
    </style>
@endsection

@section('header-content')
    <a href="{{ url()->previous() }}" class="btn btn-white m-0"><i class="fas fa-angle-left"></i> Назад</a>
@endsection

@section('content')
    <div class="row my-3">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-body">
                    <table class="table">
                        <tr>
                            <td>Имя, фамилия</td>
                            <td>{{ $client->name }} {{ $client->lastname }}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>{{ $client->email }}</td>
                        </tr>
                        <tr>
                            <td>Сайт</td>
                            <td>{{ $client->website }}</td>
                        </tr>
                        <tr>
                            <td>Страна</td>
                            <td>{{ $client->country }}</td>
                        </tr>
                        <tr>
                            <td>Город</td>
                            <td>{{ $client->city }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-12 my-5">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label class="h4">Ролики</label>
                            <ul class="list-group">
                                @foreach($client->videos as $video)
                                    <li class="list-group-item"><a href="/admin/videos/{{ $video->id }}">{{ $video->title }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
