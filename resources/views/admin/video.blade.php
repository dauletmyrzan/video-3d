@extends('layouts.admin', ['headerClass' => 'py-6', 'pageTitle' => 'Видео «' . $video->title . "»"])

@section('additional-css')
    <style>
        table td:nth-of-type(odd){
            width: 40%;
            text-align: right;
        }
        table td:nth-of-type(even){
            font-weight: bold;
        }
    </style>
@endsection

@section('header-content')
    <a href="/admin/videos" class="btn btn-white m-0"><i class="fas fa-angle-left"></i> Назад</a>
@endsection

@section('content')
    <div class="row my-3">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-body">
                    @if($video->approved)
                        <div class="badge badge-success text-center font-weight-bold mb-3" style="font-size:12pt;">Клиент утвердил видео!</div><br>
                        @if($video->filename_full)
                            <p class="text-muted">Вы уже загрузили видео.</p>
                            <p>Текст: {{ $video->director_text }}</p>
                            <a href="/admin/videos/{{ $video->id }}/upload-full" class="btn btn-success btn-sm mb-4">Загрузить новое видео без водяного знака</a>
                        @else
                            <a href="/admin/videos/{{ $video->id }}/upload-full" class="btn btn-success btn-sm mb-4">Загрузить видео без водяного знака</a>
                        @endif
                    @endif
                    @if(session('message'))
                        <div class="alert alert-success">{{ session('message') }}</div>
                    @endif
                    <table class="table">
                        <tr>
                            <td>Название</td>
                            <td>{{ $video->title }}</td>
                        </tr>
                        <tr>
                            <td>Дата создания</td>
                            <td>{{ $video->created_at }}</td>
                        </tr>
                        <tr>
                            <td>Клиент</td>
                            <td>
                                @if($video->client->name)
                                    <a href="/admin/clients/{{ $video->client->id }}">{{ $video->client->name }} {{ $video->client->lastname }}</a>
                                @else
                                    <a href="/admin/clients/{{ $video->client->id }}">{{ $video->client->email }}</a>
                                @endif
                            </td>
                        </tr>
                        @if($video->director_text)
                        <tr>
                            <td>Ваш текст клиенту</td>
                            <td>{{ $video->director_text }}</td>
                        </tr>
                        @endif
                        <tr>
                            <td>Файл</td>
                            <td>@if($video->filename)
                                    @if(filter_var($video->filename, FILTER_VALIDATE_URL) === false)
                                        <a style="text-decoration: underline;" href="{{ asset('storage/' . $video->filename) }}" download>Скачать</a><br>
                                        <a style="text-decoration: underline;" href="{{ asset('storage/' . $video->filename) }}" target="_blank">Открыть в новом окне</a>
                                    @else
                                        <a style="text-decoration: underline;" href="{{ $video->filename }}" target="_blank">Открыть в новом окне</a>
                                    @endif
                                @else
                                    -
                                @endif</td>
                        </tr>
                        <tr>
                            <td style="padding-top:27px;">Статус</td>
                            <td>
                                <form action="{{ route('set_video_status') }}" method="POST" class="form-inline">
                                    @csrf
                                    <input type="hidden" name="video_id" value="{{ $video->id }}">
                                    <select name="status" class="form-control">
                                        @foreach($statuses as $s)
                                            <option {{ $video->status_id == $s->id ? 'selected' : '' }} value="{{ $s->id }}">{{ $s->name }}</option>
                                        @endforeach
                                    </select>
                                    <button class="ml-2 btn btn-success">Сохранить</button>
                                </form>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#uploadVideoModal">Загрузить новую версию ролика</button>
                    <a href="/admin/videos/{{ $video->id }}/feedback" class="btn btn-warning float-right">Читать комментарии клиента</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="uploadVideoModal" tabindex="-1" role="dialog" aria-labelledby="uploadVideoModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="uploadVideoModalLabel">Загрузка новой версии ролика</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('upload_new_video') }}" method="POST" enctype="multipart/form-data" id="upload_new_video">
                    @csrf
                    <input type="hidden" name="video_id" value="{{ $video->id }}">
                    <div class="modal-body">
                        <label for="file">Загрузите файл: (.mp4)</label>
                        <input type="file" name="file" id="file" class="form-control mb-1">
                        <label>или</label>
                        <input type="text" name="url" id="url" placeholder="Ссылка на YouTube" class="form-control">
                        <div class="form-group mt-3">
                            <label for="director_text">Дополнительный текст клиенту:</label>
                            <textarea name="director_text" id="director_text" cols="30" rows="5" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary">Загрузить</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $("#upload_new_video").on("submit", function(){
                if($("#file").val().length === 0 && $("#url").val().length === 0){
                    alert('Загрузите видео или укажите ссылку на него.');
                    return false;
                }
            });
        });
    </script>
@endsection