@extends('layouts.admin', ['headerClass' => 'pb-8 pt-5 pt-md-8', 'pageTitle' => 'Dashboard'])

@section('header-content')
    <div class="row text-center">
        <div class="col-md-4">
            <a href="/admin/polls">
                <div class="card card-stats py-3">
                    <div class="card-body">
                        <div class="h1">Опросы</div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-4">
            <a href="/admin/videos">
                <div class="card card-stats py-3">
                    <div class="card-body">
                        <div class="h1">Ролики</div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="row mt-4 text-center">
        <div class="col-md-4">
            <a href="/admin/faq">
                <div class="card card-stats py-3">
                    <div class="card-body">
                        <div class="h1">FAQ</div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-4">
            <a href="/admin/clients">
                <div class="card card-stats py-3">
                    <div class="card-body">
                        <div class="h1">Клиенты</div>
                    </div>
                </div>
            </a>
        </div>
    </div>
@endsection

@section('content')

@endsection