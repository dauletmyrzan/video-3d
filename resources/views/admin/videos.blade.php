@extends('layouts.admin', ['headerClass' => 'py-6', 'pageTitle' => 'Ролики'])

@section('header-content')
    <a href="{{ url()->previous() }}" class="btn btn-white m-0"><i class="fas fa-angle-left"></i> Назад</a>
@endsection

@section('content')
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th>#</th>
                                <th>Название</th>
                                <th>Статус</th>
                                <th>Утвержден</th>
                                <th>Клиент</th>
                                <th>Дата начала</th>
                                <th></th>
                            </thead>
                            @foreach($videos as $key => $video)
                                <tr {{ $video->approved ? 'style=background-color:#f2f2f2' : '' }}>
                                    <td>{{ $key+1 }}</td>
                                    <td><a href="/admin/videos/{{ $video->id }}">{{ $video->title }}</a></td>
                                    <td>{{ $video->status }}</td>
                                    <td>{!! $video->approved ? '<i class="fas fa-check text-success"></i>' : '<i class="fas fa-clock text-gray"></i>' !!}</td>
                                    <td>{{ $video->client }}</td>
                                    <td>{{ $video->created_at }}</td>
                                    <td><a href="/admin/videos/{{ $video->id }}" class="btn btn-primary btn-sm">Посмотреть</a></td>
                                </tr>
                            @endforeach
                        </table>
                        {{ $videos->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
