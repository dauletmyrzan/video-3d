@extends('layouts.admin', ['headerClass' => 'py-6', 'pageTitle' => 'Опросы'])

@section('header-content')
    <a href="{{ url()->previous() }}" class="btn btn-white m-0"><i class="fas fa-angle-left"></i> Назад</a>
@endsection

@section('content')
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th>#</th>
                                <th>Автор</th>
                                <th>Дата заполнения</th>
                                <th></th>
                            </thead>
                            @foreach($polls as $key => $poll)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $poll->author }}</td>
                                    <td>{{ $poll->created_at }}</td>
                                    <td><a href="/admin/polls/{{ $poll->id }}" class="btn btn-success btn-sm">Посмотреть</a></td>
                                </tr>
                            @endforeach
                        </table>
                        {{ $polls->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
