@extends('layouts.admin', ['headerClass' => 'py-6', 'pageTitle' => 'Опрос #' . $poll->id])

@section('additional-css')
    <style>
        table td:nth-of-type(odd){
            width: 40%;
            text-align: right;
        }
        table td:nth-of-type(even){
            font-weight: bold;
        }
    </style>
@endsection

@section('header-content')
    <a href="{{ url()->previous() }}" class="btn btn-white m-0"><i class="fas fa-angle-left"></i> {{ __('buttons.back') }}</a>
@endsection

@section('content')
    <div class="row my-3">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-body">
                    <table class="table">
                        <tr>
                            <td>Дата прохождения опроса</td>
                            <td>{{ $poll->created_at }}</td>
                        </tr>
                        <tr>
                            <td>Автор</td>
                            <td>{{ $poll->author->name ?? 'No name' }} {{ $poll->author->lastname ?? 'No lastname' }}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>
                                @if($poll->author)
                                {{ $poll->author->email }}
                                <a href="/admin/clients/{{ $poll->author->id }}">(Перейти к карточке клиента)</a>
                                @else
                                    Пользователь удален
                                @endif
                            </td>
                        </tr>
                        @foreach($answers as $answer)
                        <tr>
                            <td>{{ $answer->question->question }}</td>
                            <td>{{ $answer->answer }}</td>
                        </tr>
                        @endforeach
                        <tr>
                            <td>Файл</td>
                            <td>
                                @if($poll->filename)
                                    <a href="{{ asset('storage/' . $poll->filename) }}" target="_blank">{{ $poll->filename }}</a>
                                @else
                                    Не прикреплен
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
