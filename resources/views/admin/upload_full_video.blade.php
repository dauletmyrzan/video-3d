@extends('layouts.admin', ['headerClass' => 'py-6', 'pageTitle' => 'Видео «' . $video->title . "»"])

@section('additional-css')
    <style>
        table td:nth-of-type(odd){
            width: 40%;
            text-align: right;
        }
        table td:nth-of-type(even){
            font-weight: bold;
        }
    </style>
@endsection

@section('header-content')
    <a href="/admin/videos" class="btn btn-white m-0"><i class="fas fa-angle-left"></i> Назад</a>
@endsection

@section('content')
    <div class="row my-3">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-body">
                    @if(session('message'))
                        <div class="alert alert-success">{{ session('message') }}</div>
                    @endif
                    <h1>Загрузка видео без водяного знака</h1>
                    <form action="{{ route('upload_new_video') }}" method="POST" enctype="multipart/form-data" id="upload_new_video">
                        @csrf
                        <input type="hidden" name="video_id" value="{{ $video->id }}">
                        <input type="hidden" name="is_full" value="1">
                        <div class="form-group">
                            <label for="file">Загрузите файл: (.mp4)</label>
                            <input type="file" name="file" id="file" required class="form-control mb-1">
                        </div>
                        <button class="btn btn-primary">Загрузить</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $("#upload_new_video").on("submit", function(){
                if($("#file").val().length === 0 && $("#url").val().length === 0){
                    alert('Загрузите видео или укажите ссылку на него.');
                    return false;
                }
            });
        });
    </script>
@endsection