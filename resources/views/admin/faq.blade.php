@extends('layouts.admin', ['headerClass' => 'py-6', 'pageTitle' => 'FAQ'])

@section('header-content')
    <a href="{{ url()->previous() }}" class="btn btn-white m-0"><i class="fas fa-angle-left"></i> Назад</a>
@endsection

@section('content')
    <div class="row my-3">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-header">
                    <a href="{{ route('create_faq') }}" class="btn btn-primary">Добавить вопрос</a>
                    <a href="/admin/faq/questions" class="btn btn-warning">Вопросы от клиентов</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th>#</th>
                                <th>Вопрос</th>
                                <th>Ответ</th>
                                <th></th>
                            </thead>
                            <tbody>
                            @foreach($faq as $key => $f)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $f->question }}</td>
                                    <td>{{ $f->answer }}</td>
                                    <td><a href="/admin/faq/{{ $f->id }}/edit" class="btn btn-success btn-sm">Изменить</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $faq->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection