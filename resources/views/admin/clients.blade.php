@extends('layouts.admin', ['headerClass' => 'py-6', 'pageTitle' => 'Клиенты'])

@section('header-content')
    <a href="{{ url()->previous() }}" class="btn btn-white m-0"><i class="fas fa-angle-left"></i> Назад</a>
@endsection

@section('content')
    <div class="row my-3">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th>#</th>
                                <th>Имя, Фамилия</th>
                                <th>Email</th>
                                <th>Сайт</th>
                                <th>Страна</th>
                                <th>Город</th>
                                <th></th>
                            </thead>
                            <tbody>
                            @foreach($clients as $key => $client)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $client->name }} {{ $client->lastname }}</td>
                                    <td>{{ $client->email }}</td>
                                    <td>{{ $client->website }}</td>
                                    <td>{{ $client->country }}</td>
                                    <td>{{ $client->city }}</td>
                                    <td><a href="/admin/clients/{{ $client->id }}" class="btn btn-primary btn-sm">Посмотреть</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $clients->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection