@extends('layouts.admin', ['headerClass' => 'py-6', 'pageTitle' => 'Чат #' . $chat->id])

@section('header-content')
    <a href="{{ url()->previous() }}" class="btn btn-white m-0"><i class="fas fa-angle-left"></i> Назад</a>
@endsection

@section('content')
    <div class="row my-3">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-body">
                    @if($chat->messages->count() > 0)
                        <div class="col-md-8">
                            <div style="max-height:400px;overflow-y:scroll;" id="scroll">
                                @foreach($chat->messages as $message)
                                    <div class="{{ $message->author_id == \Auth::user()->id ? 'float-right' : '' }} card mb-3 shadow d-inline-block mr-1">
                                        <div class="card-body">
                                            <span class="text-muted font-weight-bold d-block">{{ $message->author->name ?? $message->author->email }} ({{ $message->created_at }})</span>
                                            <span class="mt-2 d-block">{{ $message->message }}</span>
                                            @if($message->filename)
                                                <a href="{{ asset('storage/' . $message->filename) }}" download class="mt-2 d-block"><i class="fas fa-paperclip"></i> Прикрепленный файл</a>
                                            @endif
                                        </div>
                                    </div><br><div class="clearfix"></div>
                                @endforeach
                            </div>
                            <form action="{{ route('push_chat') }}" method="POST" class="mt-3" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="chat_id" value="{{ $chat->id }}">
                                <textarea name="message" required style="resize:none;" placeholder="Введите свой ответ..." class="form-control p-4 shadow" cols="30" rows="3"></textarea>
                                <div class="d-flex mt-3 justify-content-between">
                                    <input type="file" class="form-control" style="width:300px;" name="file">
                                    <button class="ml-3 float-right btn btn-primary btn-lg">Отправить</button>
                                </div>
                            </form>
                        </div>
                    @else
                        <div class="h3">Нет сообщений.</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <script type="text/javascript">
        $(function(){
            var wtf = $('#scroll');
            var height = wtf[0].scrollHeight;
            wtf.scrollTop(height);
        });
    </script>
@endsection