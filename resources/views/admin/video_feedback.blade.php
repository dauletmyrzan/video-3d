@extends('layouts.admin', ['headerClass' => 'py-6', 'pageTitle' => 'Комментарии к видео «' . $video->title . "»"])

@section('additional-css')
    <style>
        table td:nth-of-type(odd){
            width: 40%;
            text-align: right;
        }
        table td:nth-of-type(even){
            font-weight: bold;
        }
    </style>
@endsection

@section('header-content')
    <a href="/admin/videos/{{ request()->id }}" class="btn btn-white m-0">Назад</a>
@endsection

@section('content')
    <div class="row my-3">
        @if($comments->count() > 0)
            <div class="col-md-8">
                <div style="max-height:400px;overflow-y:scroll;" id="scroll">
                    @foreach($comments as $comment)
                        <div class="{{ $comment->author_id == \Auth::user()->id ? 'float-right' : '' }} card mb-3 shadow d-inline-block mr-1">
                            <div class="card-body">
                                <span class="text-muted font-weight-bold d-block">{{ $comment->author->name ?? $comment->author->email }} ({{ $comment->created_at }})</span>
                                <span class="mt-2 d-block">{{ $comment->message }}</span>
                            </div>
                        </div><br><div class="clearfix"></div>
                    @endforeach
                </div>
                <form action="{{ route('answer_video_feedback') }}" method="POST" class="mt-3">
                    @csrf
                    <input type="hidden" name="video_id" value="{{ $video->id }}">
                    <textarea name="message" required style="resize:none;" placeholder="Введите свой ответ..." class="form-control p-4 shadow border-0" cols="30" rows="3"></textarea>
                    <button class="float-right mt-3 btn btn-primary btn-lg">Отправить</button>
                </form>
            </div>
        @else
            <div class="card">
                <div class="card-body">Клиент еще не дал обратной связи.</div>
            </div>
        @endif
    </div>
@endsection

@section('page-scripts')
    <script type="text/javascript">
        $(function(){
            var wtf = $('#scroll');
            var height = wtf[0].scrollHeight;
            wtf.scrollTop(height);
        });
    </script>
@endsection