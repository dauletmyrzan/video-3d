<div class="shadow rounded-circle step video-processing done {{ $video->status_id == 1 ? 'current' : 'disabled' }}">
    <div class="step-text">
        <span>{{ __('statuses.1') }}</span>
    </div>
</div>
<div class="shadow rounded-circle step video-done {{ $video->status_id == 2 ? 'current done' : 'disabled' }}">
    @if($video->director_text != '')
    <div class="director-text">{{ $video->director_text }}</div>
    @endif
    <div class="step-text">
        <div class="mb-4">{{ __('statuses.2') }}</div>
        @if($video->filename && $video->status_id == 2)
            <a href="{{ asset('storage/' . $video->filename) }}" download class="text-success d-block w-90 mx-auto mb-3">Download video</a>
        @else
            <button class="btn btn-success d-block w-90 mx-auto mb-3" disabled>{{ __('statuses.' . $video->status_id) }}</button>
        @endif
        @if(!$video->approved && $video->status_id == 2)
            <button data-video_id="{{ $video->id }}" class="btn btn-outline-success d-block w-90 py-3 mx-auto mb-2 approve-video"><b>{{ __('main.approve') }} <img src="{{ asset('img/check.svg') }}"
                                                                                                                                                                   width="20"></b></button>
            <a href="/{{ app()->getLocale() }}/my-videos/{{ $video->id }}/feedback" class="text-dark"><u>{{ __('main.edit') }}</u></a>
        @elseif($video->status_id == 2)
            <a href="/{{ app()->getLocale() }}/payment" class="btn btn-danger d-block w-90 mx-auto mb-3"><i class="fas fa-shopping-cart"></i> Buy now</a>
        @endif
    </div>
</div>