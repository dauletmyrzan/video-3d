@extends('layouts.app', ['bodyClass' => 'bg'])

@section('content')
    <div class="container regular-page mt-5 profile min-vh-md-70">
        <h1 class="h2 d-inline-block">{{ __('chat.h1') }}<hr class="w-50"></h1>
        <p class="w-60">{{ __('chat.hello') }}, {{ Auth::user()->name ?? Auth::user()->email }}. {{ __('chat.desc') }}</p>
        <div class="row mt-5">
            <div class="col-md-1 mb-2">
                <img src="{{ asset('storage/' . \Auth::user()->img) }}" class="rounded-circle shadow" width="50">
            </div>
            <div class="col-md-10">
                <form action="/push_chat" class="d-block w-100" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="with-arrow">
                        <div class="attach cursor-pointer mb-3"><u>{{ __('buttons.attach') }}</u> <img class="ml-1" src="{{ asset('img/attach.svg') }}"
                                                                                                width="20"></div>
                        <input type="file" multiple style="display:none;" name="file[]">
                        <textarea name="message" required class="p-4 mb-4 form-control border-0 shadow" placeholder="{{ __('placeholders.enter_question') }}" style="resize:none;min-height: 130px;"></textarea>
                        <div class="attached mb-3 text-primary" style="display:none;"></div>
                    </div>
                    <button class="btn btn-success px-5">{{ __('buttons.send') }}</button>
                </form>
            </div>
        </div>
        <div style="margin-top:70px;">
            @if($chat)
                @foreach($chat->messages as $message)
                    @if($message->author_id == \Auth::user()->id)
                    <div class="row" style="margin-bottom:35px;">
                        <div class="col-md-10">
                            <div class="with-arrow position-relative">
                                <div class="text-muted mb-2" style="position:absolute;top:-30px;font-size:11pt;">{{ $message->author->name }} ({{ $message->date }})</div>
                                <div class="card px-4 py-2 mb-4 border-0 shadow" style="border-radius:10px">
                                    <div class="card-body">
                                        <div>{{ $message->message }}</div>
                                        @if($message->files)
                                            <div class="d-inline-block align-middle mt-2">
                                                @foreach($message->files as $file)
                                                    <a href="{{ asset('storage/' . $file) }}" download class="mb-2 cursor-pointer"><img src="{{ asset('img/attach.svg') }}" width="20" class="d-inline-block align-middle"><u> {{ $file }}</u></a>
                                                @endforeach
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1 mb-2">
                            <img src="{{ asset('storage/' . \Auth::user()->img) }}" class="rounded-circle shadow" width="50">
                        </div>
                    </div>
                    @else
                        <div class="row pb-5" style="margin-bottom:35px;">
                            <div class="col-md-1 mb-2">
                                <img src="{{ asset('storage/director.png') }}" class="rounded-circle shadow" width="50">
                            </div>
                            <div class="col-md-10">
                                <div class="with-arrow position-relative">
                                    <div class="text-muted mb-2" style="position:absolute;top:-30px;font-size:11pt;">{{ $message->author->name ?? $message->author->email }} ({{ $message->date }})</div>
                                    <div class="card px-4 py-2 mb-4 border-0 shadow" style="border-radius:10px">
                                        <div class="card-body">
                                            <div>{{ $message->message }}</div>
                                            @if($message->files)
                                                <div class="d-inline-block align-middle mt-2">
                                                    @foreach($message->files as $file)
                                                        <a href="{{ asset('storage/' . $file) }}" download class="mb-2 cursor-pointer"><img src="{{ asset('img/attach.svg') }}" width="20" class="d-inline-block align-middle"><u> {{ $file }}</u></a>
                                                    @endforeach
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            @endif
        </div>
    </div>
@endsection

@section('page-scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.attach').on('click', function(){
                $(this).parent().find(':file').trigger('click');
            });
            $(':file').on("change", function(){
                let attached = '';
                if(this.files.length > 3){
                    alert('Вы можете загрузить максимум 3 файла. Если их у вас больше, отправьте архивом.');
                    return false;
                }
                for(let i = 0; i < this.files.length; i++){
                    let file = this.files[i];
                    if(file.size > 10*1024*1024){
                        alert('Размер файла не должен превышать 10 Мб.');
                        return false;
                    }
                    attached += '<span class="d-block">' + file.name + '</span>';
                }
                $('.attached').html(attached).show();
            });
        });
    </script>
@endsection