@php
    $path = '';
    $pathInfo = request()->path();
    $pathExploded = explode("/", $pathInfo);
    if (count($pathExploded) > 1) {
        array_shift($pathExploded);
        $path = implode("/", $pathExploded);
    }

    if (count($pathExploded) == 1 && ($pathExploded[0] == 'login' || $pathExploded[0] == 'register')) {
        $rusPath = "/" . $pathExploded[0] . "?lang=ru";
        $engPath = "/" . $pathExploded[0] . "?lang=en";
    } else {
        $rusPath = "/ru/" . $path;
        $engPath = "/en/" . $path;
    }
@endphp
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="yandex-verification" content="4b173bd6de70476c" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ __('seo.title') }}</title>
    <link rel="shortcut icon" href="/img/favicon.png" type="image/png">

	<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&amp;subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans:wght@400;500;700;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/main.css?v=2.1') }}">
    <link rel="stylesheet" href="{{ asset('css/media.css') }}">

	@yield('additional-css')
</head>
<body class="{{ isset($bodyClass) ? $bodyClass : '' }}">
	<header class="{{ isset($headerClass) ? $headerClass : '' }}">
        <div class="container-fluid">
            <div class="row">
                <nav class="navbar navbar-expand-md mb-md-4 px-md-5 w-100">
                    <a class="navbar-brand" href="/{{ app()->getLocale() }}/"><img src="{{ asset('img/logo.png') }}" alt="3dvideo.com" width="150"></a>
                    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-bars"></i>
                    </button>
                    <div class="navbar-collapse collapse" id="navbarCollapse">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link {{ Request::is('/') ? 'active' : '' }}" href="/{{ app()->getLocale() }}/">{{ __('nav.home') }} <span class="sr-only">(current)</span></a>
                            </li>
                            @if(Auth::check())
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::is('video-types') ? 'active' : '' }}" href="{{ route('video_types', app()->getLocale()) }}">{{ __('nav.video_types') }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::is('my-videos') ? 'active' : '' }}" href="{{ route('my_videos', app()->getLocale()) }}">{{ __('nav.my_videos') }}</a>
                                </li>
                            @else
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::is('how-it-works') ? 'active' : '' }}" href="{{ route('how_it_works', app()->getLocale()) }}">{{ __('nav.how_it_works') }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::is('video-types') ? 'active' : '' }}" href="{{ route('video_types', app()->getLocale()) }}" tabindex="-1">{{ __('nav.video_types') }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ Request::is('about') ? 'active' : '' }}" href="{{ route('about', app()->getLocale()) }}" tabindex="-1">{{ __('nav.about') }}</a>
                                </li>
                            @endif
                        </ul>
                        @if(Auth::check())
                            <div class="dropdown profile-dropdown">
                                <button class="btn dropdown-toggle" data-hasicon="false" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="mr-2">{{ strlen(Auth::user()->name) > 0 ? Auth::user()->name : Auth::user()->email }}</span>
                                    <img src="{{ asset('storage/' . Auth   ::user()->img) }}" class="rounded-circle" width="32">
                                </button>
                                <div class="dropdown-menu text-center dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="{{ route('my', app()->getLocale()) }}">{{ __('profile.account') }}</a>
                                    <a class="dropdown-item" href="{{ route('profile', app()->getLocale()) }}">{{ __('profile.h1') }}</a>
                                    @if(\Auth::user()->is_admin)
                                    <a class="dropdown-item" href="{{ route('admin') }}">Админ. панель</a>
                                    @endif
                                    <a class="dropdown-item" href="{{ route('help', app()->getLocale()) }}">{{ __('nav.help') }}</a>
                                    <form action="/logout" method="post">
                                        @csrf
                                        <button type="submit" class="dropdown-item">{{ __('profile.sign_out') }}</button>
                                    </form>
                                </div>
                            </div>
                        @else
                            <div class="d-inline-block">
                                <a href="{{ route("login", ['lang' => app()->getLocale()]) }}" class="btn btn-outline-success login px-2 px--md3">{{ __('auth.login') }}</a>
                                <span class="d-inline-block ml-1 ml-md-2 align-middle">{{ __('auth.or') }}</span>
                                <a href="{{ route("register", ['lang' => app()->getLocale()]) }}" class="btn text-dark"><u>{{ __('auth.register_btn') }}</u></a>
                            </div>
                        @endif
                    </div>
                </nav>
            </div>
        </div>
	</header>
	<div class="main-content">
		@yield('content')
	</div>
	<footer class="{{ isset($footerMargin) ? $footerMargin : 'mt-5' }} pb-3 text-center text-md-left pt-4 pt-md-0 {{ isset($footerClass) ? $footerClass : '' }}" style="background-color: {{ isset($footerSvgColor) ? $footerSvgColor : '#fff' }}">
        @if(!Request::is("/"))
        <svg id="footer_svg" xmlns="http://www.w3.org/2000/svg" transform="translate(0-59)" xml:space="preserve" width="100%" version="1.1" style="margin-top:15px;shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
             viewBox="0 0 6127 231" xmlns:xlink="http://www.w3.org/1999/xlink">
            <g>
                <path fill="{{ isset($footerSvgColor) ? $footerSvgColor : '#fff' }}" d="M6127 318l0 -155 -367 -42c-122,-14 -247,-28 -373,-38 -45,-4 -327,-29 -380,-29 -22,0 -32,-2 -48,-3 -17,-1 -35,1 -51,0l-99 -3c-69,0 -138,0 -208,0 -37,0 -62,3 -98,3l-534 28c-15,2 -35,3 -47,4l-48 3c-20,1 -34,3 -51,3 -58,1 -173,14 -236,19l-240 19c-9,1 -17,0 -25,1 -12,1 -9,2 -23,3l-213 16c-10,0 -19,-1 -29,0l-287 19c-22,0 -30,3 -48,3 -18,1 -32,-1 -49,2 -17,2 -33,1 -50,1 -42,1 -99,7 -147,7 -57,-1 -141,6 -201,6 -70,0 -133,3 -201,3 -60,0 -248,-1 -303,-6 -15,-1 -36,0 -51,0 -22,0 -29,-3 -48,-3 -37,-1 -64,-3 -99,-3 -20,0 -26,-4 -48,-4l-384 -28c-14,-1 -33,-3 -47,-4 -59,-2 -317,-32 -372,-38l-542 -74c-27,-4 -62,-10 -88,-14l-92 -14 0 318 6127 0z"/>
            </g>
        </svg>
        @endif
        <div class="container-fluid px-md-5">
            <div class="row">
                <div class="col-md-4">
                    <img src="{{ asset('img/logo.png') }}" alt="{{ env('APP_NAME') }}" width="150">
                    <span class="mt-2 d-block text-dark">{{ __('main.copyright') }}</span>
                    <span class="d-block text-dark">&copy; {{ date('Y') }}</span>
                </div>
                <div class="col-md-2">
                    <ul>
                        <li><a class="link" href="{{ route('how_it_works', app()->getLocale()) }}">{{ __('nav.how_it_works') }}</a></li>
                        <li><a class="link" href="{{ route('video_types', app()->getLocale()) }}">{{ __('nav.video_types') }}</a></li>
                        <li><a class="link" href="{{ route('reviews', app()->getLocale()) }}">{{ __('nav.reviews') }}</a></li>
                        <li><a class="link" href="{{ route('about', app()->getLocale()) }}">{{ __('nav.about') }}</a></li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <ul>
                        <li><a class="link" href="{{ route('my_videos', app()->getLocale()) }}">{{ __('nav.my_videos') }}</a></li>
                        <li><a class="link" href="{{ route('help', app()->getLocale()) }}">{{ __('nav.help') }}</a></li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <div class="dropdown show border">
                        <a class="btn dropdown-toggle" href="/ru" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ app()->getLocale() == 'en' ? 'Choose language' : 'Выбрать язык' }}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="{{ $rusPath }}"><img src="{{ asset('img/russian.svg') }}" width="28"> Russian</a>
                            <a class="dropdown-item" href="{{ $engPath }}"><img src="{{ asset('img/english.svg') }}" width="28"> English</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
	<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.mask.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("input.phone").mask('+7(000) 000-0000', {placeholder: '+7(___) ___-____'});
        });
    </script>
    @yield('page-scripts')
</body>
</html>