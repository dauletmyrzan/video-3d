@extends('layouts.app')

@section('content')
    <div class="container regular-page my-5 profile min-vh-100">
        <h1 class="h2 d-inline-block">{{ __('poll.congrats_h1') }}<hr class="w-70"></h1>
        <div class="row mt-3 mb-1">
            <div class="col-md-3">
                <img src="{{ asset('img/gifs/11.gif') }}" width="100%">
            </div>
            <div class="col-md-6">
                <div class="card shadow border-0" style="border-radius:10px;">
                    <div class="card-body py-4">
                        {!! __('poll.thanks_text') !!}
                        @if(!Auth::check() && session()->has('poll'))
                            <div class="mt-2">{!! __('poll.fill_form') !!}</div>
                        @else
                            <div class="mt-3">Now you can check out the status of your video by clicking <a href="/{{ app()->getLocale() }}/my">this link</a>.</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @if(session()->has('poll'))
        <div class="row justify-content-center">
            <div class="col-md-4">
                @if(!Auth::check())
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul class="m-0 p-0" style="list-style-type:none;">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="h4 text-success font-weight-bolder text-center">{{ __('auth.register_btn') }}</div>
                    <form action="/saveStepsAndRegister" method="POST">
                        @csrf
                        <div class="form-group">
                            <input type="email" class="form-control text-center" name="email" required placeholder="{{ __('auth.email_placeholder') }}">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control text-center" name="password" required placeholder="{{ __('auth.password_placeholder') }}">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control text-center" name="password_re" required placeholder="{{ __('auth.password_placeholder_re') }}">
                        </div>
                        <div class="form-group">
                            <input type="checkbox" id="terms">
                            <label for="terms" style="font-size:10pt;">{!! __('docs.terms') !!}</label>
                        </div>
                        <div class="form-group">
                            <input type="checkbox" id="privacy_policy">
                            <label for="privacy_policy" style="font-size:10pt;">{!! __('docs.privacy_policy') !!}</label>
                        </div>
                        <a href="/{{ app()->getLocale() }}/steps/file" class="btn btn-light w-30">{{ __('main.back_btn') }}</a>
                        <button class="btn btn-success float-right px-3" id="submit_btn" disabled>{{ __('auth.register_btn') }}</button>
                        <div class="clearfix"></div>
                    </form>
                @endif
            </div>
        </div>
        @endif
    </div>
@endsection

@section('page-scripts')
    <script>
        $(document).ready(function(){
            $("#terms, #privacy_policy").on("change", function () {
                if($("input[type=checkbox]:checked").length === 2) {
                    $("#submit_btn").prop("disabled", false);
                } else {
                    $("#submit_btn").prop("disabled", true);
                }
            });
        });
    </script>
@endsection