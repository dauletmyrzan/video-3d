@extends('layouts.app', ['bodyClass' => 'bg'])

@section('content')
    <div class="container regular-page mt-5 min-vh-md-100">
        <h1 class="h2 d-inline-block">@lang('payment.title')<hr class="w-50"></h1>
        <div class="row mt-3">
            <div class="col-md-6" style="min-height:300px;">
                <p style="font-size:14pt;">@lang('payment.content')</p>
            </div>
        </div>
    </div>
@endsection