@extends('layouts.app', ['bodyClass' => 'bg'])

@section('content')
    <div class="container regular-page mt-5 how-it-works min-vh-md-70">
        <h1 class="h2 d-inline-block">{{ __('how_it_works.h1') }}<hr class="w-50"></h1>
        <div class="row text-center mt-4 steps-to-hide">
            <div class="col-md-6">
                <img src="{{ asset('img/you.png') }}" width="140">
                <h2>{{ __('how_it_works.you') }}</h2>
            </div>
            <div class="col-md-6">
                <img src="{{ asset('img/service.png') }}" width="145">
                <h2>{{ __('how_it_works.service') }}</h2>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-4">
                <div class="text-right how-it-works-step-1">
                    <div class="h5 text-success mb-1">{{ __('steps.1') }}</div>
                    <div class="h5 font-weight-bold mb-1">{{ __('steps.1_title') }}</div>
                    <p>{!! __('steps.1_desc') !!}</p>
                </div>
                <div class="hidden-step">
                    <div class="h5 text-success mb-1">{{ __('steps.2') }}</div>
                    <div class="h5 font-weight-bold mb-1">{{ __('steps.2_title') }}</div>
                    <p>{!! __('steps.2_desc') !!}</p>
                </div>
                <div class="text-right how-it-works-step-3">
                    <div class="h5 text-success mb-1">{{ __('steps.3') }}</div>
                    <div class="h5 font-weight-bold mb-1">{{ __('steps.3_title') }}</div>
                    <p>{!! __('steps.3_desc') !!}</p>
                </div>
            </div>
            <div class="col-md-4 d-none d-md-block">
                <img src="{{ asset('img/how_it_works_line.png') }}" width="100%">
            </div>
            <div class="col-md-4 steps-to-hide">
                <div class="how-it-works-step-2">
                    <div class="h5 text-success mb-1">{{ __('steps.2') }}</div>
                    <div class="h5 font-weight-bold mb-1">{{ __('steps.2_title') }}</div>
                    <p>{!! __('steps.2_desc') !!}</p>
                </div>
            </div>
        </div>
    </div>
@endsection