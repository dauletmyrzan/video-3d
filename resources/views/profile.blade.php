@extends('layouts.app')

@section('content')
    <div class="container regular-page mt-5 profile">
        <h1 class="h2 d-inline-block">{{ __('profile.h1') }}<hr class="w-100"></h1>
        <div class="row mt-3">
            <div class="col-md-5 d-flex align-items-center">
                <div class="align-middle">
                    <img id="photo" src="{{ asset('storage/' . $user->img) }}" width="150" class="rounded-circle shadow">
                </div>
                <div class="align-middle ml-md-5">
                    <div class="mb-3 cursor-pointer" id="set_photo"><img src="{{ asset('img/download-1.svg') }}" width="20" class="mr-2"> {{ __('profile.set_photo') }}</div>
                    <div class="mb-3 cursor-pointer" id="delete_photo"><img src="{{ asset('img/cross.svg') }}" width="15" class="mr-2" style="margin-left:2px;"> {{ __('profile.delete_photo') }}</div>
                    <form action="/upload_photo" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="file" class="d-none" name="photo">
                        <button class="btn btn-success btn-sm py-2 px-4 profile-img-save-button" style="display:none;">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="mt-5">
            @if(session('message'))
                <div class="alert alert-success">{{ session('message') }}</div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <div>{{ $error }}</div>
                    @endforeach
                </div>
            @endif
        </div>
        <div class="row mt-5">
            <div class="col-md-5 mb-5">
                <h5 class="font-weight-bold">{{ __('profile.personal_data') }}</h5>
                <form action="/save_personal_data" method="post" class="mt-4">
                    @csrf
                    <div class="form-group row">
                        <label class="col-md-5 col-form-label">{{ __('main.name') }}:</label>
                        <div class="col-md-7">
                            <input type="text" class="form-control" name="name" value="{{ $user->name }}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-5 col-form-label">{{ __('main.lastname') }}:</label>
                        <div class="col-md-7">
                            <input type="text" class="form-control" name="lastname" value="{{ $user->lastname }}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-5 col-form-label">{{ __('main.website') }}:</label>
                        <div class="col-md-7">
                            <input type="text" class="form-control" name="website" value="{{ $user->website }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-5 col-form-label">{{ __('main.country') }}:</label>
                        <div class="col-md-7">
                            <input type="text" class="form-control" name="country" value="{{ $user->country }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-5 col-form-label">{{ __('main.city') }}:</label>
                        <div class="col-md-7">
                            <input type="text" class="form-control" name="city" value="{{ $user->city }}">
                            <button class="btn btn-success mt-3 w-100">{{ __('main.save') }}</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-5">
                <h5 class="font-weight-bold">{{ __('profile.change_password') }}</h5>
                <form action="/new_password" method="post" class="mt-4">
                    @csrf
                    <div class="form-group row">
                        <label class="col-md-5 col-form-label">{{ __('main.old_password') }}:</label>
                        <div class="col-md-7">
                            <input type="password" class="form-control" name="old_password" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-5 col-form-label">{{ __('main.new_password') }}:</label>
                        <div class="col-md-7">
                            <input type="password" class="form-control" name="new_password">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-5 col-form-label">{{ __('main.confirm_password') }}:</label>
                        <div class="col-md-7">
                            <input type="password" class="form-control" name="confirm_password">
                            <button class="btn btn-success mt-3 w-100">{{ __('profile.change_password') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            function readURL(input){
                if(input.files && input.files[0]){
                    var reader = new FileReader();
                    reader.onload = function(e){
                        $('#photo').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#set_photo").on("click", function(){
                $("input[name=photo]").trigger("click");
            });
            $(':file').on("change", function(){
                let file = this.files[0];
                if(file.size > 1024*1024){
                    alert('Размер файла не должен превышать 1 Мб.');
                    return false;
                }
                readURL(this);
                $('.profile-img-save-button').show();
            });
            $("#delete_photo").on("click", function(){
                if(confirm("Вы действительно хотите удалить фотографию профиля?")){
                    $.ajax({
                        url: "/delete_photo",
                        success(){
                            window.location.reload();
                        }
                    });
                }
            });
        });
    </script>
@endsection