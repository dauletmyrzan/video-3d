@extends('layouts.app', ['bodyClass' => 'bg'])

@section('additional-css')
@endsection

@section('content')
    <div class="container mt-5 min-height-500 regular-page min-vh-md-60">
        <h1 class="h2">{{ __('video_types.h1') }}<hr class="w-15"></h1>
        <p>{{ __('video_types.text') }}</p>
        <div class="row mt-md-5 text-center">
            <div class="col-md-3 video-type">
                <a href="/{{ app()->getLocale() }}/2d-video">
                    <img src="{{ asset('/img/video_types/2d-video.png') }}" alt="{{ __('video_types.first') }}" width="100%" class="shadow rounded">
                    <div class="h6 mt-3">{{ __('video_types.first') }}</div>
                </a>
            </div>
            <div class="col-md-3 video-type">
                <a href="/{{ app()->getLocale() }}/3d-video">
                    <img src="{{ asset('/img/video_types/3d-video.png') }}" alt="{{ __('video_types.second') }}" width="100%" class="shadow rounded">
                    <div class="h6 mt-3">{{ __('video_types.second') }}</div>
                </a>
            </div>
            <div class="col-md-3 video-type">
                <a href="/{{ app()->getLocale() }}/presentation">
                    <img src="{{ asset('/img/video_types/presentation.png') }}" alt="{{ __('video_types.third') }}" width="100%" class="shadow rounded">
                    <div class="h6 mt-3">{{ __('video_types.third') }}</div>
                </a>
            </div>
            <div class="col-md-3 video-type">
                <a href="/{{ app()->getLocale() }}/promo-video">
                    <img src="{{ asset('/img/video_types/promo.png') }}" alt="{{ __('video_types.fourth') }}" width="100%" class="shadow rounded">
                    <div class="h6 mt-3">{{ __('video_types.fourth') }}</div>
                </a>
            </div>
        </div>
    </div>
@endsection