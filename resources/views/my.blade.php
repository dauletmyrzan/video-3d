@extends('layouts.app')

@section('content')
    <div class="container regular-page mt-5 profile min-vh-100">
        <h1 class="h2 d-inline-block">{{ __('my.h1') }}<hr class="w-50"></h1>
        @if($user->videos->count() > 0)
            <div class="row my-3">
                <div class="col-md-3">
                    <img src="{{ asset('img/gifs/statuses/1.gif') }}" height="250" id="status_gif">
                </div>
                <div class="col-md-6 px-lg-4">
                    <h2 id="main_title" class="text-success-darker text-uppercase h1 my-h2">...</h2>
                    <h5 class="my-h4">{{ __('my.demo_title_' . $user->videos[0]->status_id) }}</h5>
                    <div class="mt-2">
                        <p class="my-p">Time passed:</p>
                        <div class="timer-holder w-50">
                            <div id="timer" class="text-center"></div>
                            <div id="timer_helpers" class="d-flex text-center mt-1">
                                <div class="flex-fill">h</div>
                                <div class="flex-fill">m</div>
                                <div class="flex-fill">s</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top:20px;margin-bottom: 100px;height:300px;">
                <div class="col-md-3 mb-5">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <h5 class="text-center my-3">My projects</h5>
                        @foreach($user->videos as $key => $video)
                            <a class="btn btn-primary nav-video mb-3 {{ $key==0 ? 'active' : '' }}" style="font-size:10pt;" href="javascript:void(0)" data-video_id="{{ $video->id }}" role="tab" aria-controls="v-pills-home" aria-selected="true">
                                «{{ $video->title }}»
                            </a>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-9 px-4 project-timeline d-flex justify-content-around text-center">
                    <div class="shadow rounded-circle step done video-processing">
                        <div class="step-text">
                            <span>{{ __('statuses.1') }}</span>
                        </div>
                    </div>
                    <div class="shadow rounded-circle step video-done">
                        <div class="step-text">
                            <span>{{ __('statuses.2') }}</span>
                            <a href="" class="text-success"><u>Download video</u></a>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div>
                You don't have any videos yet.
            </div>
        @endif
    </div>
@endsection

@section('page-scripts')
    <script type="text/javascript" src="{{ asset('js/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/ez.countimer.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".nav-video").on("click", function(){
                let id = $(this).data("video_id");
                $(".nav-video").removeClass('active');
                $(this).addClass('active');
                $.ajax({
                    url: "/get_timeline",
                    dataType: "json",
                    data: {
                        id: id
                    },
                    success(response){
                        $(".project-timeline").html(response['html']);
                        $("#status_gif").attr("src", "/img/gifs/statuses/"+response['status_id']+".gif");
                        $("#main_title").text(response['title']);
                        if($("#timer").countimer().length){
                            $('#timer').countimer('destroy');
                        }
                        $('#timer').countimer({
                            initHours: response['timer']['h'],
                            initMinutes: response['timer']['m'],
                            initSeconds: response['timer']['s'],
                            autoStart: response['status_id'] === 1
                        });
                    }
                });
            });
            $(".nav-video").eq(0).trigger('click');

            $("body").on("click", ".approve-video", function(){
                let id = $(this).attr("data-video_id");
                $.ajax({
                    url: "/approve_video",
                    type: "POST",
                    data: {
                        _token: $("input[name=_token]").val(),
                        id: id
                    },
                    dataType: "json",
                    success(response){
                        alert(response['message']);
                        window.location.reload();
                    }
                });
            });
        });
    </script>
@endsection