@extends('layouts.app', ['bodyClass' => 'bg'])

@section('additional-css')
<style>
    p, ul li{
        font-family: 'Noto Sans JP', sans-serif;
        font-size: 13pt!important;
        font-weight: 300;
    }
</style>
@endsection

@section('content')
    <div class="container mt-5 min-height-500 regular-page min-vh-md-70">
        <h1>{{ __('about.h1') }}<hr></h1>
        <div class="row mt-md-5">
            <div class="col-md-4 px-md-4-5 px-4">
                <div class="about-slider shadow-sm">
                    <div class="about-slider-image" data-id="505"><img src="{{ asset('img/about-1.png') }}" width="100%" class="rounded"></div>
                </div>
            </div>
            <div class="col-md-8 pr-md-5">
                <p>{!! __('about.body') !!}</p>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts-old')
    <script type="text/javascript" src="{{ asset('js/slick.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.about-slider').slick({
                dots: true,
                prevArrow: '<img src="img/left-arrow.png" width="42" class="slick-arrow slick-left shadow-lg rounded-circle">',
                nextArrow: '<img src="img/right-arrow.png" width="42" class="slick-arrow slick-right shadow-lg rounded-circle">'
            });
            $('body').find('.about-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
                $('.about-slider-text').hide();
                $('.about-slider-text').eq(nextSlide).show();
            });
        });
    </script>
@endsection