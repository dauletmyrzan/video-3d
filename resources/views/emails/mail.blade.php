<img src="https://video-3d.com/img/logo.png" alt="Video-3D" width="150"><br>
<?php
$lang = isset($lang) ? $lang : 'en';
?>
<p style="font-size:15pt;font-family:sans-serif;">{!! $body !!}</p>
<a href="{{ $link ?? 'https://video-3d.com/' . $lang . '/my' }}" style="font-size:15pt;">View more</a>

<p style="font-family:sans-serif;">Video-3D team</p>