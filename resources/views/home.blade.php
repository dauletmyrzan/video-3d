@extends('layouts.app', ['bodyClass' => 'bg', 'footerMargin' => 'mt-0'])

@section('content')
<div class="container-fluid px-0 min-height-500">
    <div class="container mb-5 pb-5">
        <div class="row justify-content-center">
            <div class="col-md-8 text-center mt-md-5 main-header-section">
                <h4 class="font-weight-light">{{ __('main.h4') }}</h4>
                <h1><span class="text-success">{!! __('main.h1') !!}</h1>
                <h5>{{ __('main.h5') }}</h5>
                <form action="/post-steps/start" class="form-inline mx-auto my-5 d-block" method="POST">
                    @csrf
                    <input type="hidden" name="from_main" value="1">
                    <input type="text" required class="form-control mx-auto mr-md-3" name="answer" placeholder="{{ __('placeholders.main') }}">
                    <button class="btn btn-success">{{ __('buttons.get_video') }}</button>
                </form>
                <div class="main-header-section-hint">
                    <span>{!! __('main.hint') !!}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-white pb-5">
        <svg xmlns="http://www.w3.org/2000/svg" transform="translate(0-50)" xml:space="preserve" width="100%" version="1.1" style="margin-top:5px;shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
             viewBox="0 0 6127 231" xmlns:xlink="http://www.w3.org/1999/xlink">
            <g>
                <path fill="#fff" d="M6127 318l0 -155 -367 -42c-122,-14 -247,-28 -373,-38 -45,-4 -327,-29 -380,-29 -22,0 -32,-2 -48,-3 -17,-1 -35,1 -51,0l-99 -3c-69,0 -138,0 -208,0 -37,0 -62,3 -98,3l-534 28c-15,2 -35,3 -47,4l-48 3c-20,1 -34,3 -51,3 -58,1 -173,14 -236,19l-240 19c-9,1 -17,0 -25,1 -12,1 -9,2 -23,3l-213 16c-10,0 -19,-1 -29,0l-287 19c-22,0 -30,3 -48,3 -18,1 -32,-1 -49,2 -17,2 -33,1 -50,1 -42,1 -99,7 -147,7 -57,-1 -141,6 -201,6 -70,0 -133,3 -201,3 -60,0 -248,-1 -303,-6 -15,-1 -36,0 -51,0 -22,0 -29,-3 -48,-3 -37,-1 -64,-3 -99,-3 -20,0 -26,-4 -48,-4l-384 -28c-14,-1 -33,-3 -47,-4 -59,-2 -317,-32 -372,-38l-542 -74c-27,-4 -62,-10 -88,-14l-92 -14 0 318 6127 0z"/>
            </g>
        </svg>
        <div class="container text-center pb-5 pt-5 pt-md-2 portfolio">
            <div class="h3 font-weight-bolder">{{ __('main.h3') }}</div>
            <div class="row mt-5">
                <div class="col-md-4 mb-5">
                    <div class="ucg-holder">
                        <h4>{{ __('main.ucg_inst') }}</h4>
                        <img src="{{ asset('/img/ucg_inst.png') }}" alt="Story Format Instagram" width=80%" class="shadow">
                    </div>
                </div>
                <div class="col-md-4 mb-5">
                    <div class="ucg-holder">
                        <h4>{{ __('main.ucg_facebook') }}</h4>
                        <img src="{{ asset('/img/ucg_facebook.png') }}" alt="Story Format Facebook" width="82%" class="shadow">
                    </div>
                </div>
                <div class="col-md-4 mb-5 d-flex align-content-between flex-wrap bd-highlight mb-3">
                    <div class="ucg-holder">
                        <h4>{{ __('main.ucg_website') }}</h4>
                        <img src="{{ asset('/img/ucg_website.png') }}" alt="Web Site Format" width="100%" class="shadow">
                    </div>
                    <div class="ucg-holder">
                        <h4>{{ __('main.ucg_youtube') }}</h4>
                        <img src="{{ asset('/img/ucg_youtube.png') }}" alt="YouTube Format" width="100%" class="shadow">
                    </div>
                </div>
            </div>
            <a href="/{{ app()->getLocale() }}/steps/start"><button class="btn btn-success px-5">{{ __('buttons.get_video') }}</button></a>
        </div>
        <div class="container-fluid container-bg pt-4">
            <div class="container mt-5 min-height-500 regular-page min-vh-md-60">
                <h1 class="h2 text-center">{{ __('video_types.h1') }}</h1>
                <div class="row mt-md-5 text-center">
                    <div class="col-md-3 video-type">
                        <a href="/{{ app()->getLocale() }}/2d-video">
                            <img src="{{ asset('/img/video_types/2d-video.png') }}" alt="{{ __('video_types.first') }}" width="100%" class="shadow rounded">
                            <div class="h6 mt-3">{{ __('video_types.first') }}</div>
                        </a>
                    </div>
                    <div class="col-md-3 video-type">
                        <a href="/{{ app()->getLocale() }}/3d-video">
                            <img src="{{ asset('/img/video_types/3d-video.png') }}" alt="{{ __('video_types.second') }}" width="100%" class="shadow rounded">
                            <div class="h6 mt-3">{{ __('video_types.second') }}</div>
                        </a>
                    </div>
                    <div class="col-md-3 video-type">
                        <a href="/{{ app()->getLocale() }}/presentation">
                            <img src="{{ asset('/img/video_types/presentation.png') }}" alt="{{ __('video_types.third') }}" width="100%" class="shadow rounded">
                            <div class="h6 mt-3">{{ __('video_types.third') }}</div>
                        </a>
                    </div>
                    <div class="col-md-3 video-type">
                        <a href="/{{ app()->getLocale() }}/promo-video">
                            <img src="{{ asset('/img/video_types/promo.png') }}" alt="{{ __('video_types.fourth') }}" width="100%" class="shadow rounded">
                            <div class="h6 mt-3">{{ __('video_types.fourth') }}</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <iframe width="100%" height="500" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-scripts')
    <script type="text/javascript" src="{{ asset('js/slick.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".portfolio-video-img-holder").on("click", function(){
                let src = $(this).data("src");
                $("#videoModal").find("iframe").attr("src", src);
                $("#videoModal").modal('show');
            });
            $("#show_portfolio").on("click", function(){
                $(this).hide();
                $(".hidden-portfolio").fadeIn();
            });
        });
    </script>
@endsection