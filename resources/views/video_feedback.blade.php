@extends('layouts.app', ['bodyClass' => 'bg'])

@section('content')
    <div class="container mt-5 min-height-500 regular-page min-vh-md-60">
        <h1 class="h2">«{{ $video->title }}»<hr class="w-15"></h1>
        <div class="h4">{{ __('main.feedback_desc') }}</div>
        <div class="row mt-md-5">
            <div class="col-md-12">
                @foreach($video->comments as $comment)
                    <div class="card shadow-sm mb-4">
                        <div class="card-body">
                            <span class="text-muted font-weight-bold">You:</span>
                            <span class="text-muted font-size-10">({{ $comment->created_at }})</span>
                            <span class="d-block">{{ stripslashes($comment->message) }}</span>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <form action="/video_feedback" method="POST">
                    @csrf
                    <input type="hidden" name="video_id" value="{{ $video->id }}">
                    <div class="form-group">
                        <label>{{ __('labels.your_message') }}:</label>
                        <textarea class="form-control shadow border-0 p-4" style="resize:none;" rows="5" name="message" required></textarea>
                    </div>
                    <div class="text-right">
                        <a href="/{{ app()->getLocale() }}/my-videos" class="btn btn-light px-4 mr-2">{{ __('buttons.back') }}</a>
                        <button class="btn btn-success w-40">{{ __('buttons.send') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection