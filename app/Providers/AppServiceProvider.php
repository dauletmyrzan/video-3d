<?php

namespace App\Providers;

use App\Video;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        View::composer('*', function ($view) {
            $new_faq           = DB::table('questions')->where('seen', false)->count();
            $new_chat_messages = DB::table('chat_messages')->where('seen', false)
                ->distinct('chat_id')->count('chat_id');
            $approved_videos   = Video::where('approved', 1)->count();
            $view->with([
                'new_faq' => $new_faq,
                'new_chat_messages' => $new_chat_messages,
                'approved_videos' => $approved_videos
            ]);
        });
    }
}
