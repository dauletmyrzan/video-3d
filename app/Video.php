<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Video extends Model
{
    public function comments()
    {
        return $this->hasMany('App\VideoComment');
    }

    public function getStatusAttribute()
    {
        $status = DB::table('statuses')->where('id', $this->status_id)->value('name');
        return $status ?? '???';
    }
}
