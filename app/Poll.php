<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{
    public function questions()
    {
        return $this->hasMany(PollQuestion::class);
    }

    public function answers()
    {
        return $this->hasMany(PollAnswer::class);
    }
}
