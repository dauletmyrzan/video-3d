<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getImgAttribute($img)
    {
        return $img && $img != '' ? $img : 'login-user-icon.png';
    }

    public function getReadyVideosCount()
    {
        $count = Video::where([
            ['client_id', $this->id],
            ['status_id', 4]
        ])->count();
        return $count;
    }

    public function getVideosInProcessCount()
    {
        $count = Video::where('client_id', $this->id)
            ->whereIn('status_id', [2, 3])
            ->count();
        return $count;
    }

    public function videos()
    {
        return $this->hasMany('App\Video', 'client_id');
    }

    public function generateVideoTitle()
    {
        return "Video №" . ($this->videos()->count() + 1);
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new \App\Core\Auth\VerifyEmail);
    }
}
