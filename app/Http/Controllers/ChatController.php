<?php

namespace App\Http\Controllers;

use App\Chat;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ChatController extends Controller
{
    public function push(Request $request)
    {
        $this->validate($request, [
            'file.*' => 'mimes:doc,docx,xls,xlsx,pdf,zip,jpeg,jpg,gif,mp3,mp4,wav,txt,rar,bmp,png'
        ]);

        $user = \Auth::user();
        $filenames = array();
        if($request->hasFile('file'))
        {
            $files = $request->file('file');
            foreach($files as $file)
            {
                $filename = md5($file->getClientOriginalName()) . "." . $file->getClientOriginalExtension();
                $file->storeAs(
                    'public', $filename
                );
                $filenames[] = $filename;
            }
        }
        if($request->chat_id)
        {
            $chat = Chat::find($request->chat_id);
        }
        else
        {
            $chat = Chat::where('client_id', $user->id)->first();
        }
        if(!$chat)
        {
            $chat = new Chat;
            $chat->client_id = $user->id;
            $chat->save();
        }
        $message = addslashes($request->message);
        DB::table('chat_messages')->insert([
            'message' => $message,
            'chat_id' => $chat->id,
            'author_id' => $user->id,
            'filename' => implode(",", $filenames),
            'created_at' => date('Y-m-d H:i:s')
        ]);

        $data = array(
            'body' => $user->email . ' оставил сообщение.',
            'link' => 'https://video-3d.com/admin/chats'
        );
        $emails = User::where('is_director', 1)->pluck('email')->toArray();
        Mail::send('emails.mail', $data, function($message) use ($user, $emails){
            $message->to($emails)
                ->subject('Новое сообщение');
            $message->from('info@video-3d.com', 'Video-3D');
        });

        return back();
    }

    public function myChat()
    {
        $chat = Chat::where('client_id', \Auth::user()->id)->first();
        if($chat)
        {
            foreach($chat->messages as &$message)
            {
                $message->date = date('d.m.Y', strtotime($message->created_at))
                    . " " . date('H:i', strtotime($message->created_at));
                $message->author = User::find($message->author_id);
                $message->files = $message->filename ? explode(",", $message->filename) : '';
            }
        }
        return view('chat', compact(['chat']));
    }
}
