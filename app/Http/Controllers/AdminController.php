<?php

namespace App\Http\Controllers;

use App\Faq;
use App\Chat;
use App\Poll;
use App\User;
use App\Video;
use App\VideoComment;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Traits\HelperTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{
    use HelperTrait;

    /**
     * Индексная страница админки
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.index');
    }

    /**
     * Страница опросов
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function polls()
    {
        $polls = DB::table('polls')->orderBy('created_at', 'DESC')->paginate(15);

        foreach($polls as &$poll)
        {
            $user = User::find($poll->user_id);

            if (!$user) {
                $poll->author = 'Undefined';
            } else {
                $poll->author = $user->name ? $user->name . " " . $user->lastname : $user->email;
            }
        }

        return view('admin.polls', compact(['polls']));
    }

    /**
     * Страница опроса
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function poll(Request $request)
    {
        $poll = Poll::find($request->id);
        if($poll)
        {
            $poll->author = User::find($poll->user_id);
            $answers      = $poll->answers;

            return view('admin.poll', compact(['poll', 'answers']));
        }
        else
        {
            abort(404);
        }
    }

    /**
     * Функция ответа на отзыв видео
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function answerVideoFeedback(Request $request)
    {
        $videoId            = $request->video_id;
        $message            = addslashes($request->message);

        $comment            = new VideoComment();
        $comment->message   = $message;
        $comment->author_id = \Auth::user()->id;
        $comment->video_id  = $videoId;
        $comment->save();

        return back();
    }

    /**
     * Функция установки статуса видео
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setVideoStatus(Request $request)
    {
        $videoId          = $request->video_id;
        $video            = Video::find($videoId);
        $video->status_id = $request->status;
        $video->save();

        $user = User::find($video->client_id);
        $data = array(
            'body' => 'Your video is ready to watch!'
        );
        Mail::send('emails.mail', $data, function($message) use ($user){
            $message->to($user->email, $user->email)
                ->subject('Your video is ready!');
            $message->from('info@video-3d.com', 'Video-3D');
        });


        return back();
    }

    /**
     * Функция создания видео
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createVideo(Request $request)
    {
        $poll             = Poll::find($request->poll_id);
        $video            = new Video();
        $video->title     = $poll->title;
        $video->client_id = $poll->client_id;
        $video->status_id = 4;
        $video->hash      = md5($video->title.microtime());
        $video->poll_id   = $poll->id;
        $video->save();

        $user = User::find($video->client_id);
        $data = array(
            'body' => 'Мы начали снимать ролик по вашему сценарию!'
        );
        Mail::send('emails.mail', $data, function($message) use ($user){
            $message->to($user->email)
                ->subject('Новый ролик по вашему сценарию');
            $message->from('info@video-3d.com', 'Video-3D');
        });


        return redirect('/admin/videos/'.$video->id);
    }

    /**
     * Страница всех видео
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function videos()
    {
        $videos = DB::table('videos')->orderBy('created_at', 'DESC')->paginate(15);

        foreach($videos as &$video)
        {
            $user          = User::find($video->client_id);
            $video->client = $user->name ? $user->name . " " . $user->lastname : $user->email;
            $video->status = $this->getStatus($video->status_id);
        }

        return view('admin.videos', compact(['videos']));
    }

    /**
     * Страница одного видео
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function video(Request $request)
    {
        $video                = Video::find($request->id);
        $video->save();
        $statuses             = $this->getStatuses();

        if ($video) {
            $video->client = User::find($video->client_id);
            $video->status = $this->getStatus($video->status_id);

            return view('admin.video', compact(['video', 'statuses']));
        }

        abort(404);
    }

    /**
     * Отзывы к видео
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function videoFeedback(Request $request)
    {
        $comments = VideoComment::where('video_id', $request->id)->get();
        $video    = Video::find($request->id);

        foreach ($comments as &$comment) {
            $comment->author = User::find($comment->author_id);
        }

        return view('admin.video_feedback', compact(['video', 'comments']));
    }

    /**
     * Загрузить видеофайл
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function uploadNewVideo(Request $request)
    {
        $video_id = $request->video_id;
        $video    = Video::find($video_id);

        if ($request->hasFile('file')) {
            $this->validate($request, [
                'file.*' => 'mimes:mp4'
            ]);
            $file = $request->file('file');

            if ($video->filename && $video->filename != '') {
                $arr = explode(".", $video->filename);
                array_pop($arr);
                $file_name = implode(".", $arr);
            } else {
                $file_name = md5(microtime());
            }

            if ($request->has('is_full')) {
                $file_name .= "-full";
            }

            $file_name = $file_name . "." . $file->getClientOriginalExtension();
            $file->storeAs(
                'public', $file_name
            );
        } else {
            $file_name = $request->url;
        }

        if ($request->has('is_full')) {
            $video->filename_full = $file_name;
            $user = User::find($video->client_id);

            $data = array(
                'body' => 'Your video is ready to download without watermark on it! Click the link below:',
                'link' => 'https://video-3d.com/my-videos/'
            );

            Mail::send('emails.mail', $data, function($message) use ($user){
                $message->to($user->email, $user->email)->subject('Your video is ready to download!');
                $message->from('info@video-3d.com', 'Video-3D');
            });
        } else {
            $video->filename = $file_name;
        }

        if ($request->has('director_text')) {
            $video->director_text = $request->get('director_text');
        }

        $video->save();

        return back()->with('message', 'Видео успешно загружено.');
    }

    /**
     * Частые вопросы
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function faq()
    {
        $faq = Faq::paginate(15);

        foreach ($faq as &$f) {
            $f->answer = Str::limit($f->answer, 50);
        }

        return view('admin.faq', compact(['faq']));
    }

    /**
     * Страница редактирования частых вопросов
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editFaq(Request $request)
    {
        $faq = Faq::find($request->id);

        return view('admin.edit_faq', compact(['faq']));
    }

    /**
     * Функция обновления вопроса
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateFaq(Request $request)
    {
        $faq           = Faq::find($request->faq_id);
        $faq->question = addslashes($request->question);
        $faq->answer   = addslashes($request->answer);
        $faq->save();

        return redirect('/admin/faq');
    }

    /**
     * Страница создания вопроса
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createFaq()
    {
        return view('admin.create_faq');
    }

    /**
     * Функция сохранения вопроса
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeFaq(Request $request)
    {
        $faq           = new Faq();
        $faq->question = addslashes($request->question);
        $faq->answer   = addslashes($request->answer);
        $faq->save();

        return redirect('/admin/faq');
    }

    /**
     * Вопросы
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function questions()
    {
        $questions = DB::table('questions')->orderBy('created_at', 'DESC')->paginate(15);
        DB::table('questions')->orderBy('created_at', 'DESC')->take(15)->update(['seen' => 1]);

        foreach ($questions as &$q) {
            $q->author = User::find($q->author_id);
        }

        return view('admin.questions', compact(['questions']));
    }

    /**
     * Страница клиентов
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function clients()
    {
        $clients = User::where('is_director', 0)->paginate(15);

        return view('admin.clients', compact(['clients']));
    }

    /**
     * Страница клиента
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function client(Request $request)
    {
        $client = User::find($request->id);

        return view('admin.client', compact(['client']));
    }

    /**
     * Страница чатов
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function chats()
    {
        $chats = Chat::orderBy('id', 'DESC')->paginate(15);

        foreach ($chats as $chat) {
            $chat->author = User::find($chat->client_id);
        }

        return view('admin.chats', compact(['chats']));
    }

    /**
     * Страница чата
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function chat(Request $request)
    {
        $chat         = Chat::find($request->id);
        $chat->author = User::find($chat->client_id);
        DB::table('chat_messages')->where(['chat_id' => $chat->id])->update(['seen' => 1]);

        foreach ($chat->messages as &$message) {
            $message->author = User::find($message->author_id);
        }

        return view('admin.chat', compact(['chat']));
    }

    public function uploadFullIndex(Request $request)
    {
        $video = Video::find($request->id);
        return view('admin.upload_full_video', compact('video'));
    }
}
