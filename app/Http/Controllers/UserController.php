<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Lang;
use PDF;
use App\Poll;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        return view('my', compact(['user']));
    }

    public function edit()
    {
        $user = Auth::user();
        return view('profile', compact(['user']));
    }

    public function chat()
    {
        return view('chat');
    }

    public function uploadPhoto(Request $request)
    {
        if($request->hasFile('photo')){
            $user = \Auth::user();
            $file_name = md5($user->email) . "." . $request->file('photo')->getClientOriginalExtension();
            $user->img = $file_name;
            $user->save();
            $request->file('photo')->storeAs(
                'public', $file_name
            );
        }
        return back();
    }

    public function deletePhoto()
    {
        $user = \Auth::user();
        unlink(storage_path('app/public/' . $user->img));
        $user->img = null;
        $user->save();
    }

    public function savePersonalData(Request $request)
    {
        $user = \Auth::user();
        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->website = $request->website;
        $user->country = $request->country;
        $user->city = $request->city;
        $user->save();
        return back();
    }

    public function newPassword(Request $request)
    {
        $user = \Auth::user();
        $this->validate($request, [
            'old_password'     => 'required',
            'new_password'     => 'required|min:6',
            'confirm_password' => 'required|same:new_password',
        ]);
        $data = $request->all();
        if(!Hash::check($data['old_password'], $user->password))
        {
            return back()->withErrors(['message', 'Старый пароль не совпадает.']);
        }
        else
        {
            $user->password = Hash::make($request->new_password);
            $user->save();
        }
        return back()->with('message', 'Пароль успешно сменен.');
    }

    public function getTimeline(Request $request)
    {
        $video   = Video::find($request->id);
        $titles  = [
            1 => Lang::get('statuses.1'),
            2 => Lang::get('statuses.2')
        ];

        $date1   = strtotime($video->created_at);
        $date2   = $video->status_id == 1 ? time() : strtotime($video->updated_at);
        $diff    = abs($date2 - $date1);

        $years   = floor($diff / (365*60*60*24));
        $months  = floor(($diff - $years * 365*60*60*24)
            / (30*60*60*24));
        $days    = floor(($diff - $years * 365*60*60*24 -
                $months*30*60*60*24)/ (60*60*24));
        $hours   = floor(($diff - $years * 365*60*60*24
                - $months*30*60*60*24 - $days*60*60*24)
            / (60*60));
        $minutes = floor(($diff - $years * 365*60*60*24
                - $months*30*60*60*24 - $days*60*60*24
                - $hours*60*60)/ 60);
        $seconds = floor(($diff - $years * 365*60*60*24
            - $months*30*60*60*24 - $days*60*60*24
            - $hours*60*60 - $minutes*60));
        $json    = [
            'success'    => true,
            'html'       => view('timeline', compact(['video']))->render(),
            'title'      => $titles[$video->status_id],
            'status_id'  => $video->status_id,
            'timer'      => [
                'd' => $days,
                'h' => $hours,
                'm' => $minutes,
                's' => $seconds,
            ]
        ];
        return response()->json($json);
    }

    public function payment(Request $request)
    {
        $videoId = $request->id;
        $video   = Video::find($videoId);
        $invoice = $video->invoice;
        return view('payment', compact(['video', 'invoice']));
    }

    public function generateInvoice(Request $request)
    {
        $videoId = $request->id;
        $data = [
            'company'        => $request->company,
            'bin'            => $request->bin,
            'kbe'            => $request->kbe,
            'address'        => $request->address,
            'iban'           => $request->iban,
            'bik'            => $request->bik,
            'bank_name'      => $request->bank_name,
            'invoice_number' => 1
        ];
        $pdf            = PDF::loadView('pdf.invoice', $data);
        $filename       = 'Счет на оплату № ' . $data['invoice_number'] . '.pdf';
        $video          = Video::find($videoId);
        $video->invoice = $filename;
        $video->save();
        Storage::put('public', $filename, $pdf->output());
        return redirect('/payment/' . $videoId);
    }
}
