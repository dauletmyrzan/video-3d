<?php

namespace App\Http\Controllers;

use App\Faq;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function show(Faq $faq)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function edit(Faq $faq)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Faq $faq)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function destroy(Faq $faq)
    {
        //
    }

    public function ask(Request $request)
    {
        $message = addslashes($request->message);
        $author_id = \Auth::user()->id;
        $file_name = '';
        if($request->hasFile('file')){
            $file = $request->file('file');
            $file_name = md5($file->getClientOriginalName().microtime()) . "." . $file->getClientOriginalExtension();
            $request->file('file')->storeAs(
                'public', $file_name
            );
        }
        DB::table('questions')->insert([
            'message' => $message,
            'filename' => $file_name,
            'author_id' => $author_id,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        return back()->with('message', 'Спасибо! Мы получили ваш вопрос.');
    }

    public function help()
    {
        $questions = Faq::all();
        return view('help', compact(['questions']));
    }
}
