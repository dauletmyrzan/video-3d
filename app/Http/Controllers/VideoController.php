<?php

namespace App\Http\Controllers;

use App\User;
use App\Video;
use App\VideoComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class VideoController extends Controller
{
    public function myVideos()
    {
        $videos = Video::where('client_id', \Auth::user()->id)->get();
        return view('my_videos', compact(['videos']));
    }

    public function approveVideo(Request $request)
    {
        $video = Video::find($request->id);
        $video->approved = true;
        $video->save();

        $user = User::find($video->client_id);
        $data = array(
            'body' => $user->email . ' утвердил видео!',
            'link' => 'https://video-3d.com/admin/videos/' . $video->id
        );
        $emails = User::where('is_admin', 1)->pluck('email')->toArray();
        Mail::send('emails.mail', $data, function($message) use ($user, $emails){
            $message->to($emails)
                ->cc('imediaa@mail.ru')
                ->bcc('dauletmyrzan@gmail.com')
                ->subject('Утверждение видео');
            $message->from('info@video-3d.com', 'Video-3D');
        });

        return response()->json(['message' => 'Your video was approved! Thank you!']);
    }

    public function feedback(Request $request)
    {
        $video = Video::find($request->id);
        return view('video_feedback', compact(['video']));
    }

    public function sendFeedback(Request $request)
    {
        $id = $request->video_id;
        $message = addslashes($request->message);
        $user = \Auth::user();
        $comment = new VideoComment();
        $comment->author_id = $user->id;
        $comment->message = $message;
        $comment->video_id = $id;
        $comment->save();

        $data = array(
            'body' => $user->email . ' оставил отзыв к видео.',
            'link' => 'https://video-3d.com/admin'
        );
        $emails = User::where('is_admin', 1)->pluck('email')->toArray();
        Mail::send('emails.mail', $data, function($message) use ($user, $emails){
            $message->to($emails)
                ->cc('imediaa@mail.ru')
                ->subject('Новый отзыв к видео');
            $message->from('info@video-3d.com', 'Video-3D');
        });

        return back();
    }
}
