<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Validation\ValidationException;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $validator = Validator::make(request()->all(), [
            recaptchaFieldName() => recaptchaRuleName()
        ]);

        // check if validator fails
        if($validator->fails()) {
            throw ValidationException::withMessages([
                'recaptcha' => app()->getLocale() == 'en' ? 'ReCaptcha failed, try again.' : 'Докажите, что вы не робот.',
            ]);
        }

        $user = User::create([
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        $data = array(
            'body' => '<p>На сайте video-3d.com зарегистрировался <a href="https://video-3d.com/admin/users/'.$user->id.'">новый пользователь!</a></p>'
        );

        Mail::send('emails.mail', $data, function($message) use ($user){
            $message
                ->to('imediaa@mail.ru', 'Администратор')
                ->subject('Новый пользователь на video-3d.com');
            $message->from('info@video-3d.com', 'Video-3D');
        });

        return $user;
    }

    public function showRegistrationForm()
    {
        session(['link' => url()->previous()]);

        if (request()->has('lang')) {
            app()->setLocale(request()->get('lang'));
        }

        return view('auth.register');
    }
}
