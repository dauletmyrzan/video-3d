<?php

namespace App\Http\Controllers;

use App\Poll;
use App\PollAnswer;
use App\PollQuestion;
use App\User;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class PollController extends Controller
{
    public function storeStep(Request $request)
    {
        $lang = $request->session()->has('locale') ? $request->session()->get('locale') : app()->getLocale();
        if ($request->code == 'start' && !$request->get('from_main')) {
            return redirect('/' . $lang . '/steps/about');
        }

        $poll        = empty($request->session()->get('poll')) ? new Poll : $request->session()->get('poll');
        $pollAnswers = empty($request->session()->get('pollAnswers')) ? [] : $request->session()->get('pollAnswers');

        if (Auth::check()) {
            $poll->setAttribute('user_id', Auth::user()->getAuthIdentifier());
        }

        $answerCode   = $request->code;
        $answer       = $request->get('answer');
        $question     = PollQuestion::where('code', $answerCode)->first();

        if (!$question) {
            return back();
        }

        $questionId = $question->id;

        $pollAnswer = new PollAnswer;
        $pollAnswer->setAttribute('answer', $answer);
        $pollAnswer->setAttribute('question_id', $questionId);
        $pollAnswer->setAttribute('poll_id', $poll->getAttributeValue('id'));

        if ($answerCode != 'file') {
            $index = $answerCode == 'start' ? 'about' : $answerCode;
            $pollAnswers[$index] = $pollAnswer;
        }

        $request->session()->put('poll', $poll);
        $request->session()->put('pollAnswers', $pollAnswers);

        if ($request->has('finish')) {
            if (Auth::check()) {
                $poll->save();

                foreach ($pollAnswers as $pollAnswer) {
                    $pollAnswer->setAttribute('poll_id', $poll->getAttributeValue('id'));
                    $pollAnswer->save();
                }
                $user = User::find(Auth::id());

                $video            = new Video;
                $video->title     = $user->generateVideoTitle();
                $video->hash      = md5(microtime());
                $video->client_id = $user->id;
                $video->poll_id   = $poll->id;
                $video->filename  = $poll->filename;
                $video->save();
                
                if ($user->hasVerifiedEmail()) {
                    $data = array(
                        'body' => 'Thank you for the survey! We have just started making your video, you can track it in your profile.',
                        'link' => 'https://video-3d.com/' . $lang . '/my'
                    );

                    Mail::send('emails.mail', $data, function($message) use ($user){
                        $message->to($user->email, $user->email)
                            ->subject('Thank you for the survey!');
                        $message->from('info@video-3d.com', 'Video-3D');
                    });
                }

                $data = array(
                    'body' => 'На video-3d.com новый пройденный опрос!',
                    'link' => 'https://video-3d.com/admin/polls/' . $poll->id
                );

                Mail::send('emails.mail', $data, function($message) use ($user){
                    $message->to('imediaa@mail.ru', 'Администратор')
                        ->subject('Новый пройденный опрос на video-3d.com');
                    $message->from('info@video-3d.com', 'Video-3D');
                });

                $request->session()->forget('poll');
                $request->session()->forget('pollAnswers');
                $request->session()->flush();

                return redirect('/'.$lang.'/my');
            } else {
                return redirect('/'.$lang.'/steps/congratulations');
            }
        }

        $nextQuestion = PollQuestion::where('id', '>', $question->id)->where('code', '!=', 'start')
            ->orderBy('id', 'asc')->first();
        $nextCode     = $nextQuestion ? $nextQuestion->code : $question->code;

        if ($request->get('from_main')) {
            $nextCode = 'start';
        }

        return redirect('/'.$lang.'/steps/' . $nextCode);
    }

    public function uploadFile(Request $request)
    {
        sleep(1);

        if (empty($request->session()->get('poll'))) {
            $poll = new Poll;
        } else {
            $poll = $request->session()->get('poll');
        }

        if ($request->hasFile('file')) {
            $file         = $request->file('file');
            $extension    = $file->getClientOriginalExtension();
            $whiteListExt = ['doc','docx','pdf','zip','mp4','avi','mov','mpeg','png','jpeg','xls','xlsx','odt'];

            if (!in_array($extension, $whiteListExt)) {
                return response()->json([
                    'status' => 'fail',
                    'message' => 'Please upload file with one of these extensions: ' . implode(", ", $whiteListExt)
                ]);
            }

            $file_name = md5(microtime()) . "." . $extension;
            $file->storeAs(
                'public', $file_name
            );
            $poll->filename = $file_name;
            $request->session()->put('poll', $poll);

            return response()->json([
                'status' => 'ok',
                'message' => 'Your file was successfully uploaded! Click finish!'
            ]);
        }
        return response()->json([
            'status' => 'ok',
            'message' => 'Choose file first, then click Upload.'
        ]);
    }

    public function congratulations(Request $request)
    {
        if (empty($request->session()->get('poll'))) {
            return redirect('/');
        }

        return view('congratulations');
    }

    public function saveStepsAndRegister(Request $request)
    {
        $lang = $request->session()->has('locale') ? $request->session()->get('locale') : app()->getLocale();

        if (User::where('email', $request->email)->first()) {
            $emailExists = $request->session()->get('locale') == 'en' ? 'This e-mail address already exists.' : 'Этот email уже существует.';
            return back()->withErrors([$emailExists]);
        }

        $user             = new User();
        $user->password   = Hash::make($request->password);
        $user->email      = $request->email;
        $user->name       = $request->name;
        $user->lastname   = $request->lastname;
        $user->save();

        $user->sendEmailVerificationNotification();

        $poll             = $request->session()->get('poll');
        $pollAnswers      = $request->session()->get('pollAnswers');
        $poll->setAttribute('user_id', $user->id);
        $poll->save();

        foreach ($pollAnswers as $pollAnswer) {
            $pollAnswer->setAttribute('poll_id', $poll->getAttributeValue('id'));
            $pollAnswer->save();
        }

        $video            = new Video;
        $video->title     = $user->generateVideoTitle();
        $video->hash      = md5(microtime());
        $video->filename  = $poll->filename;
        $video->client_id = $user->id;
        $video->poll_id   = $poll->id;
        $video->save();

        $request->session()->forget('poll');
        $request->session()->forget('pollAnswers');
        $request->session()->flush();

        $data = array(
            'body' => 'Thank you for the survey! We have just started making your video, you can track it in your profile.',
            'link' => 'https://video-3d.com/' . $lang . '/my'
        );

        Mail::send('emails.mail', $data, function($message) use ($user){
            $message->to($user->email, $user->email)
                ->subject('Thank you for the survey!');
            $message->from('info@video-3d.com', 'Video-3D');
        });

        $data = array(
            'body' => '<p>На сайте video-3d.com зарегистрировался <a href="https://video-3d.com/admin/users/'.$user->id.'">новый пользователь!</a></p>',
            'lang' => $lang
        );

        Mail::send('emails.mail', $data, function($message) use ($user){
            $message
                ->to('imediaa@mail.ru', 'Администратор')
                ->subject('Новый пользователь на video-3d.com');
            $message->from('info@video-3d.com', 'Video-3D');
        });

        \Auth::loginUsingId($user->id);

        return redirect('/' . $lang . '/profile');
    }
}
