<?php

namespace App\Http\Controllers;

use App\PollQuestion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {
        return view('home');
    }

    public function about()
    {
        return view('about');
    }

    public function videoTypes()
    {
        return view('video_types');
    }

    public function myVideos()
    {
        return view('my_videos');
    }

    public function howItWorks()
    {
        return view('how_it_works');
    }

    public function video2d()
    {
        return view('2d');
    }

    public function video3d()
    {
        return view('3d');
    }

    public function videoPresentation()
    {
        return view('presentation');
    }

    public function videoPromo()
    {
        return view('promo');
    }

    public function step(Request $request)
    {
        if ($request->code) {
            $code         = $request->code;
            $question     = PollQuestion::where('code', $code)->first();
            $prevQuestion = PollQuestion::where('id', '<', $question->id)->orderBy('id', 'desc')->first();
            $prevCode     = $prevQuestion ? $prevQuestion->code : '';

            if (!$question) {
                redirect('/');
            }

            return view('step', compact(['question', 'prevCode']));
        }

        return redirect('/');
    }

    public function reviews()
    {
        return view('reviews');
    }

    public function payment()
    {
        return view('payment');
    }
}
