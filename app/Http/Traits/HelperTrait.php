<?php

namespace App\Http\Traits;
use Illuminate\Support\Facades\DB;

trait HelperTrait
{
    public function getStatus($id)
    {
        return DB::table('statuses')->where('id', $id)->value('name');
    }

    public function getStatuses()
    {
        return DB::table('statuses')->get();
    }
}