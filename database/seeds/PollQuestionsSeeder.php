<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PollQuestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            [
                'question' => 'Choose your format',
                'code'     => 'format'
            ],
            [
                'question' => 'Choose your videostyle',
                'code'     => 'videostyle'
            ],
            [
                'question' => 'Write your text for your video',
                'code'     => 'text'
            ],
            [
                'question' => 'Write your minimal budget for video',
                'code'     => 'minimal_budget'
            ],
            [
                'question' => 'Write your maximum budget for video',
                'code'     => 'maximum_budget'
            ],
            [
                'question' => 'Send link for video example you liked',
                'code'     => 'link'
            ],
        ];
        DB::table('poll_questions')->insert($arr);
    }
}
