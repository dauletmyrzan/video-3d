<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            ['name' => 'Видеоролик в разработке'],
            ['name' => 'Видеоролик готов'],
        ];
        DB::table('script_statuses')->insert($arr);
    }
}
