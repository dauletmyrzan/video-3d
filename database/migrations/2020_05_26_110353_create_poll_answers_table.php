<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePollAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poll_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('answer');
            $table->unsignedBigInteger('question_id');
            $table->unsignedBigInteger('poll_id');
            $table->foreign('question_id')->references('id')->on('poll_questions');
            $table->foreign('poll_id')->references('id')->on('polls');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('poll_answers');
    }
}
